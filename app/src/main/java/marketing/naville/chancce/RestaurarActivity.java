package marketing.naville.chancce;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import marketing.naville.chancce.activity.DrawerActivity;
import marketing.naville.chancce.activity.LoginActivity;
import marketing.naville.chancce.entidades.Usuario;

public class RestaurarActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtVoltar2;
    private EditText edtSenhaAtual;
    private EditText edtNovaSenha;
    private EditText edtConfirmarNovaSenha;
    private Button btnCancelarSenha;
    private Button btnConfirmarSenha;
    private LinearLayout lnlRestaurar1;
    private LinearLayout lnlRestaurar2;
    private LinearLayout lnlRestaurar3;
    private LinearLayout lnlRestaurar4;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    Global global = new Global();
    Usuario user = new Usuario();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurar);


        txtVoltar2 = (TextView) findViewById(R.id.txtVoltar2);
        edtSenhaAtual = (EditText) findViewById(R.id.edtSenhaAtual);
        edtNovaSenha = (EditText) findViewById(R.id.edtNovaSenha);
        edtConfirmarNovaSenha = (EditText) findViewById(R.id.edtConfirmarNovaSenha);
        btnCancelarSenha = (Button) findViewById(R.id.btnCancelarSenha);
        btnConfirmarSenha = (Button) findViewById(R.id.btnConfirmarSenha);
        lnlRestaurar1 = (LinearLayout) findViewById(R.id.lnlRestaurar1);
        lnlRestaurar2 = (LinearLayout) findViewById(R.id.lnlRestaurar2);
        lnlRestaurar3 = (LinearLayout) findViewById(R.id.lnlRestaurar3);
        lnlRestaurar4 = (LinearLayout) findViewById(R.id.lnlRestaurar4);

        requisicao = Volley.newRequestQueue(this);
        sessao = new AppShared(this);

        sessao.getId();


        txtVoltar2.setOnClickListener(this);
        edtSenhaAtual.setOnClickListener(this);
        edtNovaSenha.setOnClickListener(this);
        edtConfirmarNovaSenha.setOnClickListener(this);
        btnCancelarSenha.setOnClickListener(this);
        btnConfirmarSenha.setOnClickListener(this);
        lnlRestaurar1.setOnClickListener(this);
        lnlRestaurar2.setOnClickListener(this);
        lnlRestaurar3.setOnClickListener(this);
        lnlRestaurar4.setOnClickListener(this);


        //Tirar o teclado quando clicar fora
        lnlRestaurar1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlRestaurar2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        //Tirar o teclado quando clicar fora
        lnlRestaurar3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        //Tirar o teclado quando clicar fora
        lnlRestaurar4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


    }

    public void atualizarUsuario(final Usuario user) {
        String URL = getString(R.string.servidor) + "nova_senha";


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject objeto = null;

//
                try {
                    System.out.println("O RESPONSE É: " + response);
                    objeto = new JSONObject(response);

                    if(objeto.getInt("success") == 1){
                        Toast.makeText(getBaseContext(), objeto.getString("response"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getBaseContext(), objeto.getString("response"), Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(getBaseContext(), response,Toast.LENGTH_SHORT).show();
//
//      for (int i = 0; i < jsonarray.length(); i++) {
//                        JSONObject jsonobject = jsonarray.getJSONObject(i);
//
//                        if (response == "SENHA ALTERADA COM SUCESSO") {
//                            Toast.makeText(getBaseContext(), "SENHA ALTERADA COM SUCESSO", Toast.LENGTH_SHORT).show();
//                        }
//
//                        if (response == "Erro AO ALTERAR SENHA") {
//                            Toast.makeText(getBaseContext(), "Erro AO ALTERAR SENHA", Toast.LENGTH_SHORT).show();
//                        }
//
//                        if (response == "SENHA INCORRETA") {
//                            Toast.makeText(getBaseContext(), "SENHA INCORRETA", Toast.LENGTH_SHORT).show();
//                        }
//
//
//                    }


                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();

                parameters.put("idUsuario", sessao.getId() + "");
                parameters.put("senhaAntiga", edtSenhaAtual.getText().toString());
                parameters.put("senhaNova", edtNovaSenha.getText().toString());


                System.out.println("OS PARAMETROS SÃO: " + parameters);

                return parameters;
            }
        };

        requisicao.add(stringRequest);
    }


    @Override
    public void onClick(View view) {

        if (view == txtVoltar2) {
            finish();
        }

        if (view == btnCancelarSenha) {
            finish();
        }

        if (view == btnConfirmarSenha) {
            if (edtSenhaAtual.getText().toString().equals("")) {
                Toast.makeText(this, "O campo senha atual está vazio!", Toast.LENGTH_SHORT).show();

            } else if (edtNovaSenha.getText().toString().equals("")) {

                Toast.makeText(this, "O campo nova senha está vazio!", Toast.LENGTH_SHORT).show();


            } else if (edtConfirmarNovaSenha.getText().toString().equals("")) {

                Toast.makeText(this, "O campo confirmar nova senha está vazio!", Toast.LENGTH_SHORT).show();


            } else if (edtNovaSenha.length() <= 7 || edtConfirmarNovaSenha.length() <= 7) {
                Toast.makeText(this, "Sua nova senha precisa ter no mínimo 8 caracteres", Toast.LENGTH_SHORT).show();
            } else if (edtNovaSenha.getText().toString().equals(edtConfirmarNovaSenha.getText().toString())) {


                atualizarUsuario(user);
//                if (user.criptografarSenha(edtSenhaAtual.getText().toString()).equals(global.senhaUsuario.toString()) && edtNovaSenha.length() >= 8 && edtConfirmarNovaSenha.length() >= 8 && edtNovaSenha.getText().toString().equals(edtConfirmarNovaSenha.getText().toString())) {
//                    restaurarSenha(user.criptografarSenha(edtNovaSenha.getText().toString()), global.senhaUsuario, global.idUsuario);
//
//                } else {
//                    System.out.println("Não são iguais!");
//                    Toast.makeText(this, "A senha atual está incorreta!", Toast.LENGTH_SHORT).show();
//                    System.out.println(global.senhaUsuario);
//                }


            } else {
                Toast.makeText(this, "A nova senha e a confirmação precisam ser iguais", Toast.LENGTH_SHORT).show();


            }


        }

    }


}
