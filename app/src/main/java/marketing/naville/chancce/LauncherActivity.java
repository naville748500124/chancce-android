package marketing.naville.chancce;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import marketing.naville.chancce.activity.LoginActivity;


public class LauncherActivity extends AppCompatActivity {

    AppShared sessao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        sessao = new AppShared(this);


        //Contador para mudar do Launcher para tela inicial
        CountDownTimer timer = new CountDownTimer(3000, 3000) {
            public void onTick(long millisUntilFinished) {
                System.out.println("AGORA VAI");
            }

            public void onFinish() {
                String tutorial = sessao.getTutorial();

                if (!tutorial.equals("visto")) {


                    Intent intent = new Intent(LauncherActivity.this, IntroActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                }
            }
        };
        timer.start();
    }
}
