package marketing.naville.chancce;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by eduardo on 16/07/16.
 */

public class AppShared {
    public static final String PREFERENCE_NAME = "INFO";
    private final SharedPreferences sharedpreferences;

    public AppShared(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public String getLogin() {
        String login = sharedpreferences.getString("login", "");
        return login;
    }

    public void setLogin(String login) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("login", login);
        editor.commit();
    }


    public String getNaoAbrirAlert() {
        String naoAbrirLogin = sharedpreferences.getString("naoAbrirLogin", "abrir");
        return naoAbrirLogin;
    }

    public void setNaoAbrirLogin(String naoAbrirLogin) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("naoAbrirLogin", naoAbrirLogin);
        editor.commit();
    }

    public String getIdBloco() {
        String idBloco = sharedpreferences.getString("idBloco", "");
        return idBloco;
    }

    public void setIdBloco(String idBloco) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idBloco", idBloco);
        editor.commit();
    }


    public int getCheckBoxConectado() {
        int checkBocConectado = sharedpreferences.getInt("checkBocConectado", 0);
        return checkBocConectado;
    }

    public void setCheckBoxConectado(int checkBocConectado) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("checkBocConectado", checkBocConectado);
        editor.commit();
    }


    public String getTutorial() {
        String tutorial = sharedpreferences.getString("tutorial", "");
        return tutorial;
    }

    public void setTutorial(String login) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("tutorial", login);
        editor.commit();
    }


    public String getSenha() {
        String senha = sharedpreferences.getString("senha", "");
        return senha;
    }

    public void setSenha(String senha) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("senha", senha);
        editor.commit();
    }

    /*Campos temporários*/
    //Dados do usuário logado
    public String getUsuario() {
        String usuario = sharedpreferences.getString("usuario", "");
        return usuario;
    }

    public void setUsuario(String usuario) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("usuario", usuario);
        editor.commit();
    }

    public String getEmail() {
        String email = sharedpreferences.getString("email", "");
        return email;
    }

    public void setEmail(String email) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public int getId() {
        int id = sharedpreferences.getInt("id_usuario", 0);
        return id;
    }

    public void setId(int id) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("id_usuario", id);
        editor.commit();
    }

    public int getBadge() {
        int badge = sharedpreferences.getInt("badge_blocos", 0);
        return badge;
    }

    public void setBadge(int badge) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("badge_blocos", badge);
        editor.commit();
    }


    public int getAgencia() {
        int agencia = sharedpreferences.getInt("agencia", 0);
        return agencia;
    }

    public void setAgencia(int agencia) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("agencia", agencia);
        editor.commit();
    }

    public boolean getManterConectado() {
        boolean conectado = sharedpreferences.getBoolean("conectado", false);
        return conectado;

    }

    public void setManterConectado(boolean conectado) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("conectado", conectado);
        editor.commit();
    }

    public int getConta() {
        int conta = sharedpreferences.getInt("conta", 0);
        return conta;
    }

    public void setConta(int conta) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("conta", conta);
        editor.commit();
    }


    public String getDigito() {
        String digito = sharedpreferences.getString("digito", "");
        return digito;
    }

    public void setDigito(String digito) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("digito", digito);
        editor.commit();
    }


    public String getTelefone() {
        String telefone = sharedpreferences.getString("telefone", "");
        return telefone;
    }

    public void setTelefone(String telefone) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("telefone", telefone);
        editor.commit();
    }


    public String getCpf() {
        String cpf = sharedpreferences.getString("cpf", "");
        return cpf;
    }

    public void setCpf(String cpf) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("cpf", cpf);
        editor.commit();
    }

    //Usado nas listas para diferenciar a categoria e diretório de imagens.
    public String getCategoria() {
        String Categoria = sharedpreferences.getString("Categoria", "");
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Categoria", Categoria);
        editor.commit();
    }

    //Usado para a lista de operadoras em consulta a concorrência, guarda o valor para passar para próxima tela
    public String getOperadora() {
        String Operadora = sharedpreferences.getString("Operadora", "");
        return Operadora;
    }

    public void setOperadora(String Operadora) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Operadora", Operadora);
        editor.commit();
    }

    //Detalhes da lista
    public String getTitulo() {
        String Titulo = sharedpreferences.getString("Titulo", "");
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Titulo", Titulo);
        editor.commit();
    }

    public String getDescricao() {
        String Descricao = sharedpreferences.getString("Descricao", "");
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Descricao", Descricao);
        editor.commit();
    }

    public String getData() {
        String Data = sharedpreferences.getString("Data", "");
        return Data;
    }

    public void setData(String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Data", Data);
        editor.commit();
    }

    public String getUrlImg() {
        String UrlImg = sharedpreferences.getString("UrlImg", "");
        return UrlImg;
    }

    public void setUrlImg(String UrlImg) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("UrlImg", UrlImg);
        editor.commit();
    }

    //Checkin / Carteira
    public String getUrl() {
        String Url = sharedpreferences.getString("Url", "");
        return Url;
    }

    public void setUrl(String Url) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Url", Url);
        editor.commit();
    }

    public int getFk_rede() {
        int fk_rede = sharedpreferences.getInt("fk_rede", 0);
        return fk_rede;
    }

    public void setFk_rede(int fk_rede) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("fk_rede", fk_rede);
        editor.commit();
    }

    //ID dos comunicados, destaques, relatórios, fique atento e consulta a concorrência.
    public int getIdPost() {
        int id_post = sharedpreferences.getInt("id_post", 0);
        return id_post;
    }

    public void setIdPost(int id_post) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("id_post", id_post);
        editor.commit();
    }

    public void setFullNameFacebook(String fullNameFacebook){

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("fullNameFacebook", fullNameFacebook);
        editor.commit();
    }

    public String getFullNameFacebook(){

        return sharedpreferences.getString("fullNameFacebook", "");
    }

    public void setIdFacebook(String idFacebook){

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idFacebook", idFacebook);
        editor.commit();
    }

    public String getIdFacebook(){

        return sharedpreferences.getString("idFacebook", "");
    }

    public void setEmailFacebook(String emailFacebook){

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("emailFacebook", emailFacebook);
        editor.commit();
    }

    public String getEmailFacebook(){

        return sharedpreferences.getString("emailFacebook", "");
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("usuario");
        editor.remove("fk_grupo");
        editor.remove("id_usuario");
        editor.remove("Categoria");
        editor.remove("Operadora");
        editor.remove("Titulo");
        editor.remove("Descricao");
        editor.remove("Data");
        editor.remove("UrlImg");
        editor.remove("fk_rede");
        editor.remove("id_post");
        editor.commit();
    }

}