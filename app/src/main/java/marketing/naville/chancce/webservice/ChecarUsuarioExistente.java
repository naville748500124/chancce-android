package marketing.naville.chancce.webservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import marketing.naville.chancce.Global;
import marketing.naville.chancce.activity.DrawerActivity;
import marketing.naville.chancce.entidades.Usuario;

/**
 * Created by Xx on 22/06/2017.
 */

public class ChecarUsuarioExistente extends AsyncTask<String, Void, String> {
    private Context context;

    public ChecarUsuarioExistente(Context context) {
        this.context = context;
    }

    /**
     * Before starting background thread Show Progress Dialog
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Getting product details in background thread
     */
    protected String doInBackground(String... arg0) {
        String emailUsuario = arg0[0];

        // updating UI from Background Thread
        // Check for success tag
        int success;
        JSONParser jsonParser = new JSONParser();
        String resposta = null;

        try {
            // Building Parameters
            HashMap<String, String> data = new HashMap<String, String>();
            data.put("emailUsuario", emailUsuario);

            // getting product details by making HTTP request
            // Note that product details url will use GET request
            JSONObject json = jsonParser.makeHttpRequest("http://www.meussites.xyz/webservice/chancce/usuario/checarUsuarioExistente.php", "POST", data);
            //JSONObject json = jsonParser.makeHttpRequest("localhost/chancce/usuario/checarUsuarioExistente.php", "POST", data);
            // check your log for json response
            Log.d("Single User Details", json.toString());

            // json success tag
            success = json.getInt("success");
            if (success == 1) {
                resposta = "EXISTE";
            } else {
                resposta = "NÃO EXISTE";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resposta;
    }

    /**
     * After completing background task Dismiss the progress dialog
     **/
    protected void onPostExecute(String result) {
        String resposta = result;
        if (resposta != null) {
            if(resposta == "EXISTE") {
                Toast.makeText(context, "Usuário já cadastrado!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Erro ao cadastrar!", Toast.LENGTH_SHORT).show();
        }
    }
}
