package marketing.naville.chancce.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Xx on 23/06/2017.
 */

public class AtualizarUsuario extends AsyncTask<String, String, String> {
    private Context context;

    public AtualizarUsuario(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {

    }

    protected String doInBackground(String... arg0) {
        String nomeUsuario = arg0[0];
        String nomeUsuarioVisualizacao = arg0[1];
        String emailUsuario = arg0[2];
        String senhaUsuario = arg0[3];
        String telefoneUsuario = arg0[4];
        String bancoUsuario = arg0[5];
        String agenciaUsuario = arg0[6];
        String contaUsuario = arg0[7];
        String cpfUsuario = arg0[8];
        String idUsuario = arg0[9];
        String imagemUsuario = arg0[10];

        // Building Parameters
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("nomeUsuario", nomeUsuario);
        data.put("nomeUsuarioVisualizacao", nomeUsuarioVisualizacao);
        data.put("emailUsuario", emailUsuario);
        data.put("senhaUsuario", senhaUsuario);
        data.put("telefoneUsuario", telefoneUsuario);
        data.put("bancoUsuario", bancoUsuario);
        data.put("agenciaUsuario", agenciaUsuario);
        data.put("contaUsuario", contaUsuario);
        data.put("cpfUsuario", cpfUsuario);
        data.put("idUsuario", idUsuario);
        data.put("imagemUsuario", imagemUsuario);

        JSONParser jsonParser = new JSONParser();

        // sending modified data through http request
        // Notice that update product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest("http://www.meussites.xyz/webservice/chancce/usuario/atualizarUsuario.php", "POST", data);

        // check json success tag
        Log.d("Create Response", json.toString());

        try {
            int success = json.getInt("success");

            if (success == 1) {
                return "SUCESSO";
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * After completing background task Dismiss the progress dialog
     **/
    protected void onPostExecute(String result) {
        String jsonStr = result;
        if (jsonStr != null) {
            try {
                if (result.equals("SUCESSO")) {
                    Toast.makeText(context, "Usuário Atualizado com sucesso!", Toast.LENGTH_SHORT).show();
                } else if (result.equals("FRACASSO")) {
                    Toast.makeText(context, "Erro ao cadastrar! Tente novamente!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Não foi possível conectar ao Banco de Dados. Cheque sua conexão com a Internet!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
