package marketing.naville.chancce;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import marketing.naville.chancce.activity.LoginActivity;

public class PoliticaActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtVoltarPolitica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica);

        txtVoltarPolitica = (TextView) findViewById(R.id.txtVoltarPolitica);




        txtVoltarPolitica.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {


        if (view == txtVoltarPolitica){

            startActivity(new Intent(PoliticaActivity.this, LoginActivity.class));
            finish();


        }
    }
}
