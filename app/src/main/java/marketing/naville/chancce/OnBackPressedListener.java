package marketing.naville.chancce;

public interface OnBackPressedListener {
    void onBackPressed();
}
