package marketing.naville.chancce;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import bolts.Task;
import marketing.naville.chancce.activity.Cadastro2Activity;
import marketing.naville.chancce.activity.DrawerActivity;
import marketing.naville.chancce.activity.LoginActivity;
import marketing.naville.chancce.activity.WebService;

public class CompararActivity extends AppCompatActivity implements View.OnClickListener {

    private WebView testeWebView;
    private Button btnComparar;
    private int controlador = 1;
    private Button btnProximo;
    private TextView txtVoltarComparar1;
    private CheckBox checkOk;
    private Boolean checkBoxState;
    private CheckBox checkCampoEmBranco;
    private CheckBox checkConferenciaObrigatoriaSSP;
    private CheckBox checkCampoIlegivel;
    private CheckBox checkDataDivergente;
    String img_link_proximo;
    private TextView txtDataComparar;
    String data_canhoto;
    int id_bloco;
    TextView txtAtualTotal;
    String id_canhoto;

    String img_link;
    TimerTask task = null;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;

    String fkOpcaoOk = "";
    String fkOpcaoCamposEmBranco = "";
    String fkOpcapCampoIlegivel = "";
    String fkOpcaoConferencia = "";
    String fkOpcaoDataDivergente = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparar);


        testeWebView = (WebView) findViewById(R.id.testeWebView);
        btnComparar = (Button) findViewById(R.id.btnComparar);
        btnProximo = (Button) findViewById(R.id.btnProximo);
        txtVoltarComparar1 = (TextView) findViewById(R.id.txtVoltarComparar1);
        checkOk = (CheckBox) findViewById(R.id.checkOk);
        checkCampoEmBranco = (CheckBox) findViewById(R.id.checkCampoEmBranco);
        checkConferenciaObrigatoriaSSP = (CheckBox) findViewById(R.id.checkConferenciaObrigatoriaSSP);
        txtDataComparar = (TextView) findViewById(R.id.txtDataComparar);
        checkCampoIlegivel = (CheckBox) findViewById(R.id.checkCampoIlegivel);
        checkDataDivergente = (CheckBox) findViewById(R.id.checkDataDivergente);
        txtAtualTotal = (TextView) findViewById(R.id.txtAtualTotal);

        requisicao = Volley.newRequestQueue(this);
        sessao = new AppShared(this);


        testeWebView.getSettings().setBuiltInZoomControls(true);
        testeWebView.getSettings().setSupportZoom(true);

        testeWebView.getSettings().setJavaScriptEnabled(true);
        testeWebView.getSettings().setDomStorageEnabled(true);


        txtVoltarComparar1.setOnClickListener(this);

        checkOk.setOnClickListener(this);
        checkCampoEmBranco.setOnClickListener(this);
        checkConferenciaObrigatoriaSSP.setOnClickListener(this);
        checkCampoIlegivel.setOnClickListener(this);
        checkDataDivergente.setOnClickListener(this);


        id_bloco = Integer.parseInt(sessao.getIdBloco());

        System.out.println("O ID DO BLOCO SELECIONADO É: " + id_bloco);


        lerCanhotos();


        decrementarBarra();

        checkBoxState = checkOk.isChecked();

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessao.getNaoAbrirAlert().equals("abrir")) {
                    abrirConfrmação();
                } else {
                    atualizarCanhoto();

                }
            }
        });


    }


    public void lerCanhotos() {
        String URL = getString(R.string.servidor) + "ler_Canhotos";


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            id_canhoto = jsonObject.getString("id_canhoto");
                            img_link = jsonObject.getString("img_link");
                            data_canhoto = jsonObject.getString("data_canhoto");
                            String canhoto_atual = jsonObject.getString("canhoto_atual");
                            String canhoto_total = jsonObject.getString("canhoto_total");
                            img_link_proximo = jsonObject.getString("img_link_proximo");


                            System.out.println("O ID DO CANHOTO É: " + id_canhoto);
                            System.out.println("O LINK DA IMAGEM DO CANHOTO É: " + img_link);
                            System.out.println("A DATA DO CANHOTO É: " + data_canhoto);
                            System.out.println("O CARANHOTO ATUAL É: " + canhoto_atual);
                            System.out.println("O CARANHOTO TOTAL É: " + canhoto_total);
                            System.out.println("O LINK DO PRÓXIMO CANHOTO É: " + img_link_proximo);


                            txtAtualTotal.setText(canhoto_atual + "/" + canhoto_total);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        testeWebView.setWebViewClient(new

                                WebViewClient());
                        testeWebView.loadUrl(img_link);

                        txtDataComparar.setText(data_canhoto);
                        btnComparar.setOnClickListener(new View.OnClickListener()


                        {
                            @Override
                            public void onClick(View view) {

                                if (controlador == 1) {
                                    controlador = 2;
                                    btnComparar.setText("VOLTAR");
                                    testeWebView.setWebViewClient(new

                                            WebViewClient());
                                    testeWebView.loadUrl(img_link_proximo);

                                } else if (controlador == 2) {
                                    controlador = 1;
                                    btnComparar.setText("COMPARAR");
                                    testeWebView.loadUrl(img_link);


                                }
                            }
                        });


                    }
                }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.getMessage());

            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();

                parameters.put("id_bloco", id_bloco + "");


                System.out.println("OS PARAMETROS SÃO: " + parameters);

                return parameters;
            }
        };

        requisicao.add(stringRequest);


        btnProximo.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                String URL = getString(R.string.servidor) + "atualizar_Canhoto";


                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String id_canhoto = jsonObject.getString("id_canhoto");
                                    img_link = jsonObject.getString("img_link");
                                    String data_canhoto = jsonObject.getString("data_canhoto");
                                    String canhoto_atual = jsonObject.getString("canhoto_atual");
                                    String canhoto_total = jsonObject.getString("canhoto_total");
                                    img_link_proximo = jsonObject.getString("img_link_proximo");


                                    System.out.println("O ID DO CANHOTO É: " + id_canhoto);
                                    System.out.println("O LINK DA IMAGEM DO CANHOTO É: " + img_link);
                                    System.out.println("A DATA DO CANHOTO É: " + data_canhoto);
                                    System.out.println("O CARANHOTO ATUAL É: " + canhoto_atual);
                                    System.out.println("O CARANHOTO TOTAL É: " + canhoto_total);
                                    System.out.println("O LINK DO PRÓXIMO CANHOTO É: " + img_link_proximo);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println(error.getMessage());

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<String, String>();


                        parameters.put("fk_opcao_checklist", "1,2,3");
                        parameters.put("ws_id_canhoto", "1");
                        parameters.put("id_usuario", sessao.getId() + "");
                        parameters.put("id_bloco", id_bloco + "");


                        System.out.println("OS PARAMETROS SÃO: " + parameters);

                        return parameters;
                    }
                };

                requisicao.add(stringRequest);

            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == txtVoltarComparar1) {
            finish();
        }

        if (view == checkOk) {

            if (checkOk.isChecked()) {
                checkBoxState = true;
                checkCampoEmBranco.setEnabled(false);
                checkCampoEmBranco.setChecked(false);
                checkCampoIlegivel.setEnabled(false);
                checkCampoIlegivel.setChecked(false);
                checkDataDivergente.setEnabled(false);
                checkDataDivergente.setChecked(false);
                checkConferenciaObrigatoriaSSP.setEnabled(false);
                checkConferenciaObrigatoriaSSP.setChecked(false);

                System.out.println(checkBoxState);

            } else {
                checkBoxState = false;
                System.out.println(checkBoxState);
                checkCampoEmBranco.setEnabled(true);
                checkCampoIlegivel.setEnabled(true);
                checkDataDivergente.setEnabled(true);
                checkConferenciaObrigatoriaSSP.setEnabled(true);

            }
        }
    }

    public void decrementarBarra() {
        final ProgressBar mProgressBar;
        CountDownTimer mCountDownTimer;
        final int[] i = {0};

        final int[] contador = {100};


        mCountDownTimer = new CountDownTimer(3500, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                //Decrementa o progresso
                i[0]++;
                contador[0] = ((int) 100 - (i[0] * 10));
                btnProximo.setText("" + millisUntilFinished / 1000);
                btnProximo.setEnabled(false);


            }

            @Override
            public void onFinish() {
                //Zera o progresso ao final
                System.out.println("Encerrado");
                i[0]++;
                contador[0] = 0;
                btnProximo.setText("PRÓXIMO");
                btnProximo.setEnabled(true);

                //parar();
            }
        };
        mCountDownTimer.start();
    }

    public void abrirConfrmação() {

        String dataDivergente = "";
        String campoIlegivel = "";
        String campoEmBranco = "";
        String campoOk = "";
        String campoConferencia = "";

        if (checkDataDivergente.isChecked()) {
            dataDivergente = checkDataDivergente.getText().toString();
            fkOpcaoDataDivergente = "6,";

        }
        if (checkCampoIlegivel.isChecked()) {
            campoIlegivel = checkCampoIlegivel.getText().toString();
            fkOpcapCampoIlegivel = "5,";
        }
        if (checkCampoEmBranco.isChecked()) {
            campoEmBranco = checkCampoEmBranco.getText().toString();
            fkOpcaoCamposEmBranco = "4,";
        }
        if (checkOk.isChecked()) {
            campoOk = checkOk.getText().toString();
            fkOpcaoOk = "3,";
        }
        if (checkConferenciaObrigatoriaSSP.isChecked()) {
            campoConferencia = checkConferenciaObrigatoriaSSP.getText().toString();
            fkOpcaoConferencia = "7,";
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Antes de enviar confirme o que foi marcado: \n" + campoOk + "\n" + dataDivergente + "\n" + campoIlegivel + "\n" + campoEmBranco + "\n" + campoConferencia)
                .setCancelable(false)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        atualizarCanhoto();
                        sessao.setNaoAbrirLogin("abrir");


                    }
                })
                .setNegativeButton("Confirmar e interromper aviso", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        atualizarCanhoto();
                        sessao.setNaoAbrirLogin("naoAbrir");

                    }
                })
                .setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void atualizarCanhoto() {

        System.out.println("ATUALIZAR CANHOTO");


        Map<String, String> parametros = new HashMap<>();


        parametros.put("fk_opcao_checklist", fkOpcaoOk + fkOpcaoCamposEmBranco + fkOpcapCampoIlegivel + fkOpcaoConferencia + fkOpcaoDataDivergente);
        parametros.put("ws_id_canhoto", id_canhoto);
        parametros.put("id_usuario", sessao.getId() + "");
        parametros.put("id_bloco", id_bloco + "");


        WebService ws = new WebService(
                getString(R.string.servidor),
                "atualizar_Canhoto_Android",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");


                    System.out.println("o objeto é = " + objeto);


                    System.out.println("STATUS DO CADASTRO PASSAGEIRO É = " + status);


                    if (status == 1) {
                        Intent intent = new Intent(CompararActivity.this, CompararActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        finish();
                    } else if (status == -1) {
                        Toast.makeText(CompararActivity.this, "Você completou esse bloco de canhoto.", Toast.LENGTH_SHORT).show();
                        finish();
//                        Intent intent = new Intent(CompararActivity.this, DrawerActivity.class);
//                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

}


