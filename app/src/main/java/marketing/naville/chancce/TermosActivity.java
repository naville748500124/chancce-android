package marketing.naville.chancce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class TermosActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtVoltarTermos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termos);


        txtVoltarTermos = (TextView) findViewById(R.id.txtVoltarTermos);



        txtVoltarTermos.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        if (view == txtVoltarTermos){
            finish();
        }

    }
}
