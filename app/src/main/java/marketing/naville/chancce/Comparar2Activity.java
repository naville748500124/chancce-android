package marketing.naville.chancce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class Comparar2Activity extends AppCompatActivity implements View.OnClickListener{

    private WebView testeWebView;
    private Button btnComparar2;
    private int controlador = 0;
    private Button btnProximo2;
    private TextView txtVoltarComparar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparar2);


        testeWebView = (WebView) findViewById(R.id.testeWebView);
        btnComparar2 = (Button) findViewById(R.id.btnComparar2);
        btnProximo2 = (Button) findViewById(R.id.btnProximo2);
        txtVoltarComparar2 = (TextView) findViewById(R.id.txtVoltarComparar2);


        txtVoltarComparar2.setOnClickListener(this);


        btnComparar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (controlador == 1) {
                    controlador = 2;
                    testeWebView.loadUrl("http://mobile.diaslog.com.br/api/Canhoto/22598857");
                }

                else{
                    controlador = 1;
                    testeWebView.loadUrl("http://mobile.diaslog.com.br/api/Canhoto/22598872");


                }
            }
        });

        btnProximo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        testeWebView.setWebViewClient(new WebViewClient());
        testeWebView.loadUrl("http://mobile.diaslog.com.br/api/Canhoto/22598872");


        //initiate a check box
        CheckBox simpleCheckBox = (CheckBox) findViewById(R.id.simpleCheckBox);

//check current state of a check box (true or false)
        Boolean checkBoxState = simpleCheckBox.isChecked();

    }

    @Override
    public void onClick(View view) {

        if (view == txtVoltarComparar2){
            finish();
        }

    }
}
