package marketing.naville.chancce.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.TimeUnit;

import marketing.naville.chancce.R;


public class CustomAdapterOportunidadesAberto extends BaseAdapter {
    public JSONArray values;
    private static LayoutInflater inflater = null;

    public CustomAdapterOportunidadesAberto(FragmentActivity oportunidadesAbertoActivity, JSONArray values) {
        this.values = values;
        inflater = (LayoutInflater) oportunidadesAbertoActivity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return values.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class Holder {
        TextView oportunidade;
        TextView data;
        TextView tempo;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder = new Holder();
        View rowView = inflater.inflate(R.layout.oportunidades_aberto_lista, null);

        holder.oportunidade = rowView.findViewById(R.id.txtOportunidadeAberto);
        holder.data = rowView.findViewById(R.id.txtDataAberto);
        holder.tempo = rowView.findViewById(R.id.txtTempoRestanteAberto);


        try {
            holder.oportunidade.setText(values.getJSONObject(position).getString("id_bloco"));
            holder.data.setText(values.getJSONObject(position).getString("data_bloco"));
//            holder.tempo.setText(values.getJSONObject(position).getString("tempo_restante"));

            new CountDownTimer(values.getJSONObject(position).getInt("tempo_restante") * 1000, 1000) {

                public void onTick(long millisUntilFinished) {

                    System.out.println("tempo: " + millisUntilFinished);

                    holder.tempo.setText("" + String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                }

                public void onFinish() {
                    holder.tempo.setText("00:00:00");
                    System.out.println("Encerrar.");
                }
            }.start();


        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }


        return rowView;
    }

}
