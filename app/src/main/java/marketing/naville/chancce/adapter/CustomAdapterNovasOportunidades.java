package marketing.naville.chancce.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.R;
import marketing.naville.chancce.activity.SaibaMaisActivity;

public class CustomAdapterNovasOportunidades extends BaseAdapter {

    Context context;
    private final JSONArray values;
    private static LayoutInflater inflater = null;
    AppShared sessao;

    public CustomAdapterNovasOportunidades(FragmentActivity novasOportunidadesActivity, JSONArray values) {
        this.values = values;
        context = novasOportunidadesActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return values.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView oportunidade;
        TextView data;
        TextView valor;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        final View rowView = inflater.inflate(R.layout.novas_oportunidades_lista, null);

        holder.oportunidade = rowView.findViewById(R.id.txtOportunidade);
        holder.data = rowView.findViewById(R.id.txtData);
        holder.valor = rowView.findViewById(R.id.txtValor);


        try {


            holder.oportunidade.setText(values.getJSONObject(position).getString("id_bloco"));
            holder.valor.setText(values.getJSONObject(position).getString("valor").replace(".", ","));
            holder.data.setText(values.getJSONObject(position).getString("data_bloco"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                sessao = new AppShared(context);

                try {
                    System.out.println("O ID DO BLOCO É " + values.getJSONObject(position).getInt("id_bloco"));
                    final int id_do_bloco = values.getJSONObject(position).getInt("id_bloco");

                    if (sessao.getBadge() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(rowView.getContext());
                        builder.setMessage("Você só poderá pegar uma oportunidade por vez, tem certeza que deseja pegar essa oportunidade?")
                                .setCancelable(false)
                                .setPositiveButton("Saiba Mais", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                        Intent intent = new Intent(context, SaibaMaisActivity.class);
                                        intent.putExtra("id_do_bloco", id_do_bloco);
                                        context.startActivity(intent);


//
//
//                                    context.startActivity(new Intent(context, SaibaMaisActivity.class));
//                                    context.putExtra("date", date);
//                                    context.putExtra("id_usuario", id_usuario);
//                                    //lstNovasOportunidades.removeViewAt(i);
                                    }
                                })
                                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                    }else{

                        AlertDialog.Builder builder = new AlertDialog.Builder(rowView.getContext());
                        builder.setMessage("Você só pode pegar um bloco de unidade por vez.")

                                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();



                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return rowView;
    }

}
