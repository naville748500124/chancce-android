package marketing.naville.chancce.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import marketing.naville.chancce.R;

public class CustomAdapterRelatorios extends BaseAdapter{
    String [] oportunidades;
    String [] datas;
    Context context;
    private static LayoutInflater inflater = null;

    public CustomAdapterRelatorios(FragmentActivity relatoriosActivity, String[] oportunidades, String[] datas) {
        this.oportunidades = oportunidades;
        this.datas = datas;
        context = relatoriosActivity;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return oportunidades.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView oportunidade;
        TextView data;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView = inflater.inflate(R.layout.relatorios_lista, null);

        holder.oportunidade = rowView.findViewById(R.id.txtRelatorio);
        holder.oportunidade.setText("Analisar " + oportunidades[position] + " Via PDF");

        holder.data = rowView.findViewById(R.id.txtData) ;
        holder.data.setText(datas[position]);

        /*rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked "+ oportunidades[position], Toast.LENGTH_LONG).show();
            }
        });*/
        return rowView;
    }

}
