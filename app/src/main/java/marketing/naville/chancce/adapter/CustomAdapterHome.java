package marketing.naville.chancce.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import marketing.naville.chancce.Global;
import marketing.naville.chancce.R;

/**
 * Created by Alex on 13/06/2017.
 */

public class CustomAdapterHome extends BaseAdapter {



    private final JSONArray values;
    Context context;
    private static LayoutInflater inflater = null;

    public CustomAdapterHome(FragmentActivity homeActivity, JSONArray values) {
        this.values = values;

        context = homeActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return values.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView oportunidade;
        TextView data;
        TextView desconto;
        TextView subtotal;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView = inflater.inflate(R.layout.home_lista, null);

        holder.oportunidade = rowView.findViewById(R.id.txtHomeOportunidade);
        holder.data = rowView.findViewById(R.id.txtHomeData);
        holder.desconto = rowView.findViewById(R.id.txtHomeDesconto);
        holder.subtotal = rowView.findViewById(R.id.txtHomeSubtotal);


        try {


            double valor_desconto = values.getJSONObject(position).getDouble("valor_desconto");
            String valor_desconto_string = String.valueOf(valor_desconto).replace(".",",") + "0";




            double valor_pos_auditado = values.getJSONObject(position).getDouble("valor_pos_auditado");
            String valor_pos_auditado_string = String.valueOf(valor_pos_auditado).replace(".",",");

            holder.oportunidade.setText(values.getJSONObject(position).getString("id_bloco"));
            holder.data.setText(values.getJSONObject(position).getString("data_bloco"));
            holder.desconto.setText(valor_desconto_string);
            holder.subtotal.setText(valor_pos_auditado_string);


//            holder.valor.setText(values.getJSONObject(position).getString("valor").replace(".", ","));


        } catch (JSONException e) {
            e.printStackTrace();
        }

//        rowView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "You Clicked " + oportunidades[position], Toast.LENGTH_LONG).show();
//            }
//        });
        return rowView;
    }

}
