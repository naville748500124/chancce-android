package marketing.naville.chancce.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import marketing.naville.chancce.R;

/**
 * Created by Alex on 13/06/2017.
 */

public class CustomAdapterGanhos extends BaseAdapter {

    Context context;
    private final JSONArray values;
    private static LayoutInflater inflater = null;

    public CustomAdapterGanhos(FragmentActivity ganhosActivity, JSONArray values) {
        this.values = values;
        context = ganhosActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return values.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView oportunidade;
        TextView data;
        TextView valor;
        TextView ganhos;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView = inflater.inflate(R.layout.ganhos_lista, null);

        holder.oportunidade = rowView.findViewById(R.id.txtGanhosOportunidade);
        holder.data = rowView.findViewById(R.id.txtDataGanhos);
        holder.valor = rowView.findViewById(R.id.txtValorGanhos);
        holder.ganhos = rowView.findViewById(R.id.txtCanhotosGanhos);


        try {


            holder.oportunidade.setText(values.getJSONObject(position).getString("id_bloco"));
            holder.data.setText("Data do bloco " + values.getJSONObject(position).getString("data_bloco"));
            holder.valor.setText(values.getJSONObject(position).getString("valor").replace(".", ","));
            holder.ganhos.setText("Foram auditados " + values.getJSONObject(position).getString("qtd_canhotos") + " de um total de " + values.getJSONObject(position).getString("auditados") + " canhotos");


        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked "+ oportunidades[position], Toast.LENGTH_LONG).show();
            }
        });*/
        return rowView;
    }

}
