package marketing.naville.chancce.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.R;
import marketing.naville.chancce.StringEncryption;
import marketing.naville.chancce.entidades.Usuario;
import marketing.naville.chancce.fragment.PerfilFragment2;

import marketing.naville.chancce.webservice.ChecarUsuarioExistente;

public class CadastroActivity extends FragmentActivity implements View.OnClickListener {
    private Button btnFinalizar;
    private int tela;
    private String mensagemBanco = "";
    private ImageView imgFoto;
    private TextView txtVoltar;
    int requiredSize = 200;
    String imagemBase64;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    String base64Final;
    Dialog alertDialog_show;

    public static int RESULT_GALLERY = 0;

    private int RESULT_LOAD_IMG = 1;
    private int REQUEST_IMAGE_CAPTURE = 2;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        LinearLayout lnlCadastro = findViewById(R.id.lnlCadastro);

        requisicao = Volley.newRequestQueue(this);

        btnFinalizar = findViewById(R.id.btnFinalizar);
        imgFoto = findViewById(R.id.img_foto);
        txtVoltar = findViewById(R.id.txtVoltar);

//        if (!Objects.equals(GlobalConfiguracoes.linkFacebook, "")) {
////            Picasso.with(this).load(GlobalConfiguracoes.linkFacebook).into(imgFoto);
//
//        }

//
//        PerfilFragment2 perfilFragment2 = new PerfilFragment2();
//
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.fragmentCadastro, perfilFragment2, "fragmentPerfil2");
//        fragmentTransaction.commit();

        tela = 0;

        lnlCadastro.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        btnFinalizar.setOnClickListener(this);
        imgFoto.setOnClickListener(this);
        txtVoltar.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        LoginManager.getInstance().logOut();

        startActivity(new Intent(CadastroActivity.this, LoginActivity.class));
        finish();

    }

    @Override
    public void onClick(View view) {

        if (view == txtVoltar) {

            LoginManager.getInstance().logOut();

            startActivity(new Intent(CadastroActivity.this, LoginActivity.class));
            finish();

        } else if (view == btnFinalizar) {

            //validação de campos


            if (Global.nomeUsuarioVisualizacao.equals("")) {
                Toast.makeText(this, "O campo Nome está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.emailUsuario.equals("")) {
                Toast.makeText(this, "O campo E-mail está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.senhaUsuario.equals("")) {
                Toast.makeText(this, "O campo Senha está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.telefoneUsuario.equals("")) {
                Toast.makeText(this, "O campo Telefone está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.bancoUsuario == 0) {
                Toast.makeText(this, "O campo Banco está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.contaUsuario.equals("")) {
                Toast.makeText(this, "O campo Conta está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.digitoUsuario.equals("")) {
                Toast.makeText(this, "O campo Digito está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.agenciaUsuario.equals("")) {
                Toast.makeText(this, "O campo Agência está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.cpfUsuario.equals("")) {
                Toast.makeText(this, "O campo CPF está vazio.", Toast.LENGTH_SHORT).show();
            } else if (Global.senhaUsuario.length() <= 7) {
                Toast.makeText(this, "Sua senha precisa ter no mínimo 8 caracteres.", Toast.LENGTH_SHORT).show();
            } else if (Global.agenciaUsuario.length() <= 2) {
                Toast.makeText(this, "O campo Agência precisa ter no mínimo 3 dígitos.", Toast.LENGTH_SHORT).show();
            } else if (Global.contaUsuario.length() <= 4) {
                Toast.makeText(this, "O campo Conta deve ter no mínimo 4 números.", Toast.LENGTH_SHORT).show();
            } else {
                Usuario usuario = new Usuario();

                usuario.setNomeUsuario(Global.nomeUsuarioVisualizacao.toLowerCase());
                usuario.setNomeUsuarioVisualizacao(Global.nomeUsuarioVisualizacao);
                usuario.setEmailUsuario(Global.emailUsuario);
                usuario.setSenhaUsuario(Global.senhaUsuario);
                usuario.setTelefoneUsuario(Global.telefoneUsuario);
                usuario.setBancoUsuario(Global.bancoUsuario);
                usuario.setContaUsuario(Global.contaUsuario);
                usuario.setDigitoUsuario(Global.digitoUsuario);
                usuario.setAgenciausuario(Global.agenciaUsuario);
                usuario.setCpfUsuario(Global.cpfUsuario);
                usuario.setImagemUsuario(Global.imagemUsurario);

//                cadastrarUsuario(usuario);


                String usuarioExiste = "";
                try {
                    usuarioExiste = new ChecarUsuarioExistente(this).execute(usuario.getEmailUsuario()).get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                System.out.println(usuarioExiste);

//                if (!usuarioExiste.equals("EXISTE")) {
//                    cadastrarUsuario(usuario);
//
//                }

//                if (!usuarioExiste.equals("EXISTE")) {
//                    if (!usuario.checarEmail(usuario.getEmailUsuario())) {
//                        Toast.makeText(this, "Email inválido! Tente novamente.", Toast.LENGTH_SHORT).show();
//                    } else {
//                        cadastrarUsuario(usuario);
//
//                        if (mensagemBanco.equals("CADASTRADO COM SUCESSO")) {
//                            Toast.makeText(this, "Usuário cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
//                            finish();
//                        } else if (mensagemBanco.equals("OPA! DEU RUIM")) {
//                            Toast.makeText(this, "Erro  ao Conectar com o Banco de Dados. Tente novamente", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }

//                finish();
            }
        } else if (view == imgFoto) {

            chamaImagem();

        }

    }


    ProgressDialog dialog;

    private void comecarLoad() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO HOME");


    }


//    public void cadastrarUsuario(final Usuario user) {
//        String URL = getString(R.string.servidor) + "novo_Usuario";
//
//        comecarLoad();
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onResponse(String response) {
//
////                JSONObject objeto = null;
//
//
//                try {
//                    System.out.println("O RESPONSE É: " + response);
//                    jsonarray = new JSONArray(response);
//
//                    for (int i = 0; i < jsonarray.length(); i++) {
//                        JSONObject jsonobject = jsonarray.getJSONObject(i);
//                        String success = jsonobject.getString("success");
//
//
//                        System.out.println("A RESPOSTA DO JSON É: " + success);
//
//                        if (Objects.equals(success, "0")) {
//                            Toast.makeText(getBaseContext(), "O cadastro não foi efetuado, tente novamente.", Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            Toast.makeText(getBaseContext(), "O cadastro foi efetuado com sucesso.", Toast.LENGTH_SHORT).show();
//                            finish();
//                        }
//
//
//                    }
//
//                    dialog.dismiss();
//
//
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//                    dialog.dismiss();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                System.out.println(error.getMessage());
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> parameters = new HashMap<String, String>();
//
//                parameters.put("nomeUsuario", user.getNomeUsuario());
//                parameters.put("nomeUsuarioVisualizacao", user.getNomeUsuario().toLowerCase());
//                try {
//                    parameters.put("senhaUsuario", StringEncryption.SHA1(user.getSenhaUsuario())); //ATENCAO AQUI
//                } catch (NoSuchAlgorithmException e) {
//                    e.printStackTrace();
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                parameters.put("emailUsuario", user.getEmailUsuario());
//                parameters.put("telefoneUsuario", user.getTelefoneUsuario());
//                parameters.put("bancoUsuario", user.getBancoUsuario() + "");
//                parameters.put("agenciaUsuario", user.getAgenciausuario());
//                parameters.put("contaUsuario", user.getContaUsuario());
//                parameters.put("digitoUsuario", user.getDigitoUsuario());
//                parameters.put("cpfUsuario", user.getCpfUsuario());
//                parameters.put("imagemUsuario", base64Final);
//                parameters.put("id_facebook", "");
//                parameters.put("link_facebook", "");
//
//                System.out.println("OS PARAMETROS SÃO: " + parameters);
//
//                return parameters;
//            }
//        };
//
//        requisicao.add(stringRequest);
//    }

    private void chamaImagem() {

        //MODAL VIEW TERMOS DE USO
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogbox = CadastroActivity.this.getLayoutInflater().inflate(R.layout.dialog_camera_sd, null);

        ImageView camera = dialogbox.findViewById(R.id.btn_camera);
        ImageView galeria = dialogbox.findViewById(R.id.btn_galeria);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
//        final TextView tv_titulo = dialogbox.findViewById(R.id.tv_titulo_historico_procedimento);

        builder.setView(dialogbox);
        alertDialog_show = builder.create();
        alertDialog_show.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog_show.show();


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(CadastroActivity.this.getBaseContext().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    alertDialog_show.dismiss();
                }

            }
        });

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, RESULT_LOAD_IMG);
                alertDialog_show.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_show.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        Log.w("Entrei", "OnResult");

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            Log.w("Entrei", "Galeria");
            try {

                final Uri imageUri = data.getData();
                final Bitmap selectedImage = decodeUri(imageUri);

                InputStream inputStream = this.getContentResolver().openInputStream(imageUri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                Log.w("Tamanhos da imagem", "Heigth = " + imageHeight + " : Width = " + imageWidth);
                if (imageHeight <= 150 || imageWidth <= 150) {
//                    ToolBox.ExibeMSG("Imagem muito pequena!", this);
                    Toast.makeText(this, "Imagem muito pequena, seleione outra.", Toast.LENGTH_SHORT).show();
                } else {
                    String base64 = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG);

                    System.gc();

                    GuardaImagem(selectedImage);
                    System.out.println("O BASE 64 DA GALERIA É " + base64);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(CadastroActivity.this, "Você não selecionou uma foto.", Toast.LENGTH_LONG).show();

            }


        } else if (resultCode == RESULT_OK && reqCode == REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String base64 = encodeToBase64(imageBitmap, Bitmap.CompressFormat.JPEG);

            System.gc();
            GuardaImagem(imageBitmap);

            System.out.println("O BASE 64 DA CAMERA É " + base64);
        } else {
            Toast.makeText(CadastroActivity.this, "Foto não Capturada.", Toast.LENGTH_LONG).show();
        }
        System.gc();


    }


    public Bitmap decodeUri(Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(CadastroActivity.this.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(CadastroActivity.this.getContentResolver().openInputStream(uri), null, o2);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }


    private void GuardaImagem(Bitmap bitmap) {

        String nova_imagem64 = Imagem_encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG);
        System.out.println("A STRING NOVA IMAGEM É " + nova_imagem64);
        base64Final = nova_imagem64;


        if (nova_imagem64.length() > 0) {
            Bitmap bitmap2 = Imagem_decodeBase64(nova_imagem64);
            imgFoto.setImageBitmap(bitmap2);
        }

    }

    public static String Imagem_encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Função para Decodificar a imagem de String para Imagem Novamente
     */
    public static Bitmap Imagem_decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


}