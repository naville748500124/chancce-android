package marketing.naville.chancce.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.R;
import marketing.naville.chancce.StringEncryption;
import marketing.naville.chancce.entidades.Usuario;
import marketing.naville.chancce.fragment.GanhosFragment;
import marketing.naville.chancce.fragment.HomeFragment;

import static android.R.id.message;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtUsuario;
    private EditText edtSenha;
    private Button btnLogin;
    private TextView txtObtenhaAjuda;
    private CheckBox chkManterConectado;

    private CallbackManager callbackManager;
    AppShared sessao;
    String texto;
    int checkBoxClicado = 0;


    private Button txtCadastro;
    private Usuario usuarioLogin = new Usuario();

    private TextView txtContato;

    //requer no webservice
    RequestQueue requisicao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //requer no webservice
        requisicao = Volley.newRequestQueue(this);

        callbackManager = CallbackManager.Factory.create();

        txtContato = (TextView) findViewById(R.id.txtContato);

        chkManterConectado = (CheckBox) findViewById(R.id.chkManterConectado);


        txtContato.setOnClickListener(this);

        sessao = new AppShared(this);


        chkManterConectado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chkManterConectado.isChecked()) {
                    checkBoxClicado = 1;
                    sessao.setCheckBoxConectado(checkBoxClicado);
                    System.out.println("O CHECKBOX TA CLICADASSO E O VALOR É QUAL?? " + sessao.getCheckBoxConectado());
                } else {
                    checkBoxClicado = 0;
                    sessao.setCheckBoxConectado(checkBoxClicado);
                    System.out.println("O CHECK BOX TA DESCLICADASSO E O VALOR É QUAL? " + sessao.getCheckBoxConectado());
                }
            }
        });


        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
//        loginButton.setVisibility(View.GONE);


        if (sessao.getManterConectado()) {
            edtUsuario.setText(sessao.getEmail());
            edtSenha.setText(sessao.getSenha());
        }


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

//                getOftalmoFromFacebook(loginResult.getAccessToken());
                System.out.println("Sucesso com facebook" + loginResult.toString());

                informacoesFaceBook();

//                Intent intent = new Intent(LoginActivity.this, GanhosFragment.class);
//                startActivity(intent);
//                finish();

            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Login com o Facebook cancelado.", Toast.LENGTH_SHORT).show();
                System.out.println("erro");

            }

            @Override
            public void onError(FacebookException error) {

                Toast.makeText(getApplicationContext(), "Erro na conexão com o Facebook.", Toast.LENGTH_SHORT).show();
                System.out.println("Sucesso com facebook" + error.toString());

            }
        });


        LinearLayout lnlLogin = (LinearLayout) findViewById(R.id.lnlLogin);
        edtUsuario = (EditText) findViewById(R.id.edtUsuario);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtObtenhaAjuda = (TextView) findViewById(R.id.txtObtenhaAjuda);

        txtCadastro = (Button) findViewById(R.id.txtCadastro);


        lnlLogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });
        btnLogin.setOnClickListener(this);
        txtObtenhaAjuda.setOnClickListener(this);

        txtCadastro.setOnClickListener(this);

//        edtUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                    String salvarUser = edtUsuario.getText().toString().trim();
//                    sessao.setLogin(salvarUser);
//
//                }
//            }
//        });
//
//        edtSenha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                    String salvarSenha = edtSenha.getText().toString().trim();
//                    sessao.setSenha(salvarSenha);
//
//
//                }
//            }
//        });

        entrarDireto();


    }


//    private void goMainScreen() {
//
//        Intent intent = new Intent(this, Cadastro2Activity.class);
//        startActivity(intent);
//    }

    ProgressDialog dialog;

    private void comecarLoad() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO HOME");


    }

    public void informacoesFaceBook(){

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                System.out.println("Entrou no informacoesFaceBook");
                try {
                    System.out.println("informaçoes do objeto" + object);

                    String nameFacebook = object.getString("first_name");
                    String fullNameFacebook = nameFacebook + object.getString("last_name");
                    String emailFacebook = object.getString("email");
                    String idFacebook = object.getString("id");

                    System.out.println("name = "+nameFacebook);
                    System.out.println("full name = "+fullNameFacebook);
                    System.out.println("email = "+emailFacebook);
                    System.out.println("id = "+idFacebook);

                    sessao.setFullNameFacebook(fullNameFacebook);
                    sessao.setIdFacebook(idFacebook);
                    sessao.setEmailFacebook(emailFacebook);

                    AccessToken accessToken = AccessToken.getCurrentAccessToken();

                    System.out.println("TOKEN FACEBOOK " + accessToken);

                    boolean isLoggedId = accessToken != null && !accessToken.isExpired();

                    if (isLoggedId){

                        login_facebook(idFacebook);

//                        Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
//                        startActivity(intent);
//                        finish();

                    }else {
                        Intent intent = new Intent(LoginActivity.this, Cadastro2Activity.class);
                        startActivity(intent);
                        finish();

                    }

                }catch (Exception e){

                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG);
                    e.getLocalizedMessage();
                }



            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }

    private void login_facebook(final String idFacebook) {

        String url = getString(R.string.servidor) + "login_facebook";

        comecarLoad();

        System.out.println(url);

//        sessao.setLogin(edtUsuario.getText().toString());
//        sessao.setSenha(edtSenha.getText().toString());

//
//        System.out.println("O EMAIL DA SESSAO É =" + sessao.getLogin());
//        System.out.println("A SENHA DA SESSAO É = " + sessao.getSenha());


        requisicao.start();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int resposta = 0;
//                        System.out.println("Logando Usuário existente");
//                        System.out.println(user.getNomeUsuario() + " " + user.getSenhaUsuario());

                        System.out.println(response);

                        try {
                            JSONObject jResposta = new JSONObject(response);
                            resposta = jResposta.getInt("idUsuario");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        sessao.setIdFacebook(idFacebook);
                        if (resposta < 1) {

                            Intent intent = new Intent(LoginActivity.this, Cadastro2Activity.class);
//                            intent.putExtra("USUARIO", usuarioLogin);
                            startActivity(intent);

                            Toast.makeText(LoginActivity.this, "Usuário e/ou senha inválidos!", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        }else try {
                            JSONObject jsonObject = new JSONObject(response);


                            sessao.setId(jsonObject.getInt("idUsuario"));
                            sessao.setUsuario(jsonObject.getString("nomeUsuario"));
                            sessao.setEmail(jsonObject.getString("emailUsuario"));
                            sessao.setTelefone(jsonObject.getString("telefoneUsuario"));
                            sessao.setAgencia(jsonObject.getInt("agenciaUsuario"));
                            sessao.setConta(jsonObject.getInt("contaUsuario"));
                            sessao.setDigito(jsonObject.getString("digitoUsuario"));
                            sessao.setCpf(jsonObject.getString("cpfUsuario"));
                            sessao.setBadge(jsonObject.getInt("badge_blocos"));
                            sessao.setUrlImg(jsonObject.getString("imagemUsuario"));

                            System.out.println("USUARIOOOOOOOOO " + sessao.getUsuario());

                            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                            intent.putExtra("USUARIO", usuarioLogin);
                            startActivity(intent);

                            dialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            dialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(LoginActivity.this, "Erro ao Conectar com o Banco de Dados. Tente novamente", Toast.LENGTH_SHORT).show();
//                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

//                params.put("emailUsuario", user.getEmailUsuario());
                params.put("id_facebook", idFacebook);

                return params;
            }
        };

        requisicao.add(stringRequest);


    }


//    public void getOftalmoFromFacebook(AccessToken at) {
//
//        GraphRequest request = GraphRequest.newMeRequest(at, new GraphRequest.GraphJSONObjectCallback() {
//
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//
////                Bundle bFacebookData = getFacebookData(object);
////                String nomeFacebookString = bFacebookData.getString("first_name");
////                String emailFacebookString = bFacebookData.getString("email");
////                String idFacebookString = bFacebookData.getString("idFacebook");
////                String genderFacebookString = bFacebookData.getString("gender");
////                String linkFacebook = ("https://graph.facebook.com/" + idFacebookString + "/picture?width=200&height=150");
////
////                Log.i("facebookName", nomeFacebookString);
////                Log.i("idFacebook", idFacebookString);
////
////
////                GlobalConfiguracoes.nomeUsuario = nomeFacebookString;
////                GlobalConfiguracoes.emailUsuario = emailFacebookString;
////                GlobalConfiguracoes.idFacebook = idFacebookString;
////                GlobalConfiguracoes.linkFacebook = linkFacebook;
//
////
////                goMainScreen();
//
//                try {
//
//                    System.out.println("Informaçoes do objeto" + object.toString());
//
//
//                }catch (Exception e){
//                    e.getMessage();
//                }
//
//
//            }
//        });

//    }

    private Bundle getFacebookData(JSONObject object) {

//        try {
//            Bundle bundle = new Bundle();
//            String id = object.getString("id");
//
//            try {
//                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
//                bundle.putString("profile_pic", profile_pic.toString());
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                return null;
//            }
//
//            bundle.putString("idFacebook", id);
//            if (object.has("first_name"))
//                bundle.putString("first_name", object.getString("first_name"));
//            if (object.has("last_name"))
//                bundle.putString("last_name", object.getString("last_name"));
//            if (object.has("email"))
//                bundle.putString("email", object.getString("email"));
//            if (object.has("gender"))
//                bundle.putString("gender", object.getString("gender"));
//            if (object.has("birthday"))
//                bundle.putString("birthday", object.getString("birthday"));
//            if (object.has("location"))
//                bundle.putString("location", object.getJSONObject("location").getString("name"));
//
//            return bundle;
//        } catch (JSONException e) {
//            Log.d("Facebook error", "Error parsing JSON");
//        }
        return null;
    }

    public static void main(String[] args) throws ParseException {
        long millis = 3600000;
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        System.out.println(hms);
    }

//    @Override
//    protected void onStart() {
////
////        AccessToken accessToken = AccessToken.getCurrentAccessToken();
////        boolean isLoggedId = accessToken != null && !accessToken.isExpired();
////
////        if (isLoggedId){
////
////            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
////            startActivity(intent);
////            finish();
////
////        }
////
////        super.onStart();
//    }

    public void onClick(View v) {
        if (v == btnLogin) {
            final Usuario usuario = new Usuario();

            String emailUsuario = edtUsuario.getText().toString();
            String senhaUsuario = null;
            try {
                senhaUsuario = StringEncryption.SHA1(edtSenha.getText().toString());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            usuario.setEmailUsuario(emailUsuario);
            usuario.setSenhaUsuario(senhaUsuario);

            if (emailUsuario.equals("") || senhaUsuario.equals("")) {
                Toast.makeText(this, "Digite todos os campos", Toast.LENGTH_SHORT).show();
            } else if (!emailUsuario.equals("") && !senhaUsuario.equals("")) {
                loginUsuario(usuario);


            }

        } else if (v == txtCadastro) {
            Intent intent = new Intent(this, Cadastro2Activity.class);
            startActivity(intent);
        } else if (v == txtObtenhaAjuda) {
            Intent intent = new Intent(this, EsqueciSenhaActivity.class);
            startActivity(intent);
        } else if (v == txtContato) {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "contato@chancce.com.br", null));
            String subject = "Contato Chancce";
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(Intent.createChooser(intent, "Choose an Email client :"));
        }
    }


    public void loginUsuario(final Usuario user) {
        String url = getString(R.string.servidor) + "loginApp";

        comecarLoad();

        System.out.println(url);

        sessao.setLogin(edtUsuario.getText().toString());
        sessao.setSenha(edtSenha.getText().toString());


        System.out.println("O EMAIL DA SESSAO É =" + sessao.getLogin());
        System.out.println("A SENHA DA SESSAO É = " + sessao.getSenha());


        requisicao.start();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int resposta = 0;
                        System.out.println("Logando Usuário existente");
                        System.out.println(user.getNomeUsuario() + " " + user.getSenhaUsuario());

                        System.out.println(response);

                        try {
                            JSONObject jResposta = new JSONObject(response);
                            resposta = jResposta.getInt("idUsuario");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (resposta == 0) {
                            Toast.makeText(LoginActivity.this, "Usuário e/ou senha inválidos!", Toast.LENGTH_SHORT).show();
                        } else try {
                            JSONObject jsonObject = new JSONObject(response);


                            sessao.setId(jsonObject.getInt("idUsuario"));
                            sessao.setUsuario(jsonObject.getString("nomeUsuario"));
                            sessao.setEmail(jsonObject.getString("emailUsuario"));
                            sessao.setTelefone(jsonObject.getString("telefoneUsuario"));
                            sessao.setAgencia(jsonObject.getInt("agenciaUsuario"));
                            sessao.setConta(jsonObject.getInt("contaUsuario"));
                            sessao.setDigito(jsonObject.getString("digitoUsuario"));
                            sessao.setCpf(jsonObject.getString("cpfUsuario"));
                            sessao.setBadge(jsonObject.getInt("badge_blocos"));
                            sessao.setUrlImg(jsonObject.getString("imagemUsuario"));


                            System.out.println("Valor do usuarioLogin = " + usuarioLogin.getIdUsuario());

                            System.out.println("O ID DO USUARIO É = " + sessao.getId());


                            edtUsuario.setText(texto);

//                            edtUsuario.setText(sessao.getLogin());
//                            edtSenha.setText(sessao.getSenha());

                            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                            intent.putExtra("USUARIO", usuarioLogin);
                            startActivity(intent);

                            dialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            dialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(LoginActivity.this, "Erro ao Conectar com o Banco de Dados. Tente novamente", Toast.LENGTH_SHORT).show();
//                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("emailUsuario", user.getEmailUsuario());
                params.put("senhaUsuario", user.getSenhaUsuario());

                return params;
            }
        };

        requisicao.add(stringRequest);
    }


    public void entrarDireto() {
        int verificaEntrar;
        verificaEntrar = sessao.getCheckBoxConectado();

        if (verificaEntrar == 0) {
            edtUsuario.setText("");
            edtSenha.setText("");
            chkManterConectado.setChecked(false);
        } else if (verificaEntrar == 1) {
            edtUsuario.setText(sessao.getLogin());
            edtSenha.setText(sessao.getSenha());
            chkManterConectado.setChecked(true);

            final Usuario usuario = new Usuario();

            String emailUsuario = edtUsuario.getText().toString();
            String senhaUsuario = null;
            try {
                senhaUsuario = StringEncryption.SHA1(edtSenha.getText().toString());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            usuario.setEmailUsuario(emailUsuario);
            usuario.setSenhaUsuario(senhaUsuario);

            if (emailUsuario.equals("") || senhaUsuario.equals("")) {
                Toast.makeText(this, "Digite todos os campos", Toast.LENGTH_SHORT).show();
            } else if (!emailUsuario.equals("") && !senhaUsuario.equals("")) {
                loginUsuario(usuario);


            }

            loginUsuario(usuario);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
