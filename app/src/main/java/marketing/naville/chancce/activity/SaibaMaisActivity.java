package marketing.naville.chancce.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.CompararActivity;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.R;
import marketing.naville.chancce.adapter.CustomAdapterNovasOportunidades;
import marketing.naville.chancce.fragment.HomeFragment;
import marketing.naville.chancce.fragment.MenuInferiorFragment;
import marketing.naville.chancce.fragment.NovasOportunidadesFragment;
import marketing.naville.chancce.fragment.OportunidadesAbertoFragment;


public class SaibaMaisActivity extends AppCompatActivity implements View.OnClickListener, NovasOportunidadesFragment.OnFragmentInteractionListener {

    private Button btnCancelarCanhoto;
    private Button btnPegarCanhoto;
    private TextView txtVoltarSaibaMais;
    private WebView wbvSaibaMais;

    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;

    int id_do_bloco;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saiba_mais);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        requisicao = Volley.newRequestQueue(this);
        sessao = new AppShared(this);


        btnCancelarCanhoto = (Button) findViewById(R.id.btnCancelarCanhoto);
        btnPegarCanhoto = (Button) findViewById(R.id.btnPegarCanhoto);
        txtVoltarSaibaMais = (TextView) findViewById(R.id.txtVoltarSaibaMais);
        wbvSaibaMais = (WebView) findViewById(R.id.wbvSaibaMais);


        btnCancelarCanhoto.setOnClickListener(this);
        btnPegarCanhoto.setOnClickListener(this);
        txtVoltarSaibaMais.setOnClickListener(this);
        wbvSaibaMais.setOnClickListener(this);

        wbvSaibaMais.setWebViewClient(new WebViewClient());
        wbvSaibaMais.loadUrl("http://31.220.63.171/chancce_painel/tutorial_app.php");


        Bundle extra = getIntent().getExtras();
        System.out.println("Passando via EXTRA " + extra.getInt("id_do_bloco"));
        id_do_bloco = extra.getInt("id_do_bloco");



        System.out.println("O ID DO BLOCO SAIBA MAIS É: " + id_do_bloco);


    }

    @Override
    public void onClick(View view) {

        if (view == btnCancelarCanhoto) {

            finish();

        } else if (view == btnPegarCanhoto) {


            String URL = getString(R.string.servidor) + "selecionou_Bloco";


            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

//                JSONObject objeto = null;


                    try {
                        JSONObject jResposta = new JSONObject(response);

                        int status = jResposta.getInt("status");
                        int badge_blocos = jResposta.getInt("badge_blocos");


                        System.out.println("O STATUS DO BLOCO É: " + status);
                        System.out.println("O VALOR DO BADGE DO BLOCOS: " + badge_blocos);

                        sessao.setBadge(1);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//                        jsonarray = new JSONArray(response);
//                        for (int i = 0; i < jsonarray.length(); i++) {
//                            JSONObject jsonobject = jsonarray.getJSONObject(i);
//                            boolean status = jsonobject.getBoolean("status");
//                            int badge_blocos = jsonobject.getInt("badge_blocos");

                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(error.getMessage());

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> parameters = new HashMap<String, String>();
//                parameters.put("id_usuario", sessao.getId() + "");
                    parameters.put("id_usuario", sessao.getId() + "");
                    parameters.put("id_bloco", id_do_bloco + "");

                    return parameters;
                }

            };

            requisicao.add(stringRequest);




            finish();


        }


        if (view == txtVoltarSaibaMais) {
            finish();
        }


    }
}
