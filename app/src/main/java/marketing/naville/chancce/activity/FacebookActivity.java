package marketing.naville.chancce.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.R;

import static marketing.naville.chancce.R.id.imageView;

public class FacebookActivity extends AppCompatActivity {

    private TextView txtNomeFacebook;
    private TextView txtEmailFacebook;
    private TextView txtIdFacebook;
    private TextView txtLinkFacebook;
    private ImageView imgPerfilFacebook;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);

        txtNomeFacebook = (TextView) findViewById(R.id.txtNomeFacebook);
        txtEmailFacebook = (TextView) findViewById(R.id.txtEmailFacebook);
        txtIdFacebook = (TextView) findViewById(R.id.txtIdFacebook);
        txtLinkFacebook = (TextView) findViewById(R.id.txtLinkFacbeook);
        imgPerfilFacebook = (ImageView) findViewById(R.id.imgPerfilFacebook);

        txtNomeFacebook.setText(GlobalConfiguracoes.nomeFacebook);
        txtEmailFacebook.setText(GlobalConfiguracoes.emailFacebook);
        txtIdFacebook.setText(GlobalConfiguracoes.idFacebook);
        txtLinkFacebook.setText(GlobalConfiguracoes.linkFacebook);

        Picasso.with(this).load(GlobalConfiguracoes.linkFacebook).into(imgPerfilFacebook);


    }
}
