package marketing.naville.chancce.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.entidades.Usuario;
import marketing.naville.chancce.fragment.ConfiguracoesFragment;
import marketing.naville.chancce.fragment.GanhosFragment;
import marketing.naville.chancce.fragment.HomeFragment;
import marketing.naville.chancce.fragment.MenuInferiorFragment;
import marketing.naville.chancce.fragment.NovasOportunidadesFragment;
import marketing.naville.chancce.fragment.OportunidadesAbertoFragment;
import marketing.naville.chancce.R;
import marketing.naville.chancce.fragment.RelatoriosFragment;

public class DrawerActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener,
        HomeFragment.OnFragmentInteractionListener, NovasOportunidadesFragment.OnFragmentInteractionListener,
        MenuInferiorFragment.OnFragmentInteractionListener, OportunidadesAbertoFragment.OnFragmentInteractionListener,
        RelatoriosFragment.OnFragmentInteractionListener, GanhosFragment.OnFragmentInteractionListener {

    private NavigationView nav_view;
    AppShared sessao;

    private ImageView imgSair;

    private static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        imgSair = (ImageView) findViewById(R.id.imgSair);

        nav_view = (NavigationView) findViewById(R.id.nav_view);
        View hView = nav_view.getHeaderView(0);
        TextView nav_user = hView.findViewById(R.id.txtNome);
        Usuario usuario = (Usuario) getIntent().getSerializableExtra("USUARIO");
        Global.nomeUsuarioVisualizacao = usuario.getNomeUsuarioVisualizacaoo();

        sessao = new AppShared(this);


        imgSair.setOnClickListener(this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment homeFragment = new HomeFragment();
        Fragment menuInferior = new MenuInferiorFragment();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.flContent, homeFragment);
        fragmentTransaction.add(R.id.fragmentMenuInferior, menuInferior);
        fragmentTransaction.commit();

//        Toast.makeText(DrawerActivity.this, "kkkkkkkkkkkkkkk", Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_novasOportunidades) {
            fragment = new NovasOportunidadesFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_oortunidadesAberto) {
            fragment = new OportunidadesAbertoFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_meusGanhos) {
            fragment = new GanhosFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_relatorios) {
            fragment = new RelatoriosFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_configuracoes) {
            fragment = new ConfiguracoesFragment();
            mudarFragment(fragment);
        } else if (id == R.id.nav_sair) {
            finish();
        }

        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

//    public static void setFragment(){
//
//      Fragment  fragment = new OportunidadesAbertoFragment();
//        mudarFragment(fragment);
//
//    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Você realmente deseja sair?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sessao.setLogin("");
                        sessao.setSenha("");
                        sessao.setCheckBoxConectado(0);
                        Intent intent = new Intent(DrawerActivity.this, LoginActivity.class);
                        startActivity(intent);
                        DrawerActivity.this.finish();

                        LoginManager.getInstance().logOut();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void mudarFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View view) {

        if (view == imgSair) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Você realmente deseja sair?")
                    .setCancelable(false)
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            sessao.setLogin("");
                            sessao.setSenha("");
                            sessao.setCheckBoxConectado(0);
                            Intent intent = new Intent(DrawerActivity.this, LoginActivity.class);
                            startActivity(intent);
                            DrawerActivity.this.finish();
                            LoginManager.getInstance().logOut();
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }


    }
}
