package marketing.naville.chancce.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import marketing.naville.chancce.R;

public class CanhotoActivity extends AppCompatActivity {
    private WebView wbvCanhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canhoto);



        wbvCanhoto = (WebView) findViewById(R.id.wbvCanhoto);

        wbvCanhoto.setWebViewClient(new WebViewClient());
        wbvCanhoto.loadUrl("http://mobile.diaslog.com.br/api/Canhoto/22603483");
    }
}
