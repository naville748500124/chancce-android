package marketing.naville.chancce.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Mask;
import marketing.naville.chancce.R;
import marketing.naville.chancce.StringEncryption;

public class Cadastro2Activity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout lnlCadastro2;
    LinearLayout lnlCadastroTopo;
    LinearLayout lnlCadastroCorpo;
    LinearLayout lnlCadastroFoto;
    TextView txtVoltarCadastro;
    CircleImageView imgCadastrarFoto;
    EditText edtCadastrarNome;
    EditText edtCadastrarEmail;
    EditText edtCadastrarSenha;
    EditText edtCadastrarTelefone;
    Spinner spnBanco2;
    EditText edtCadastrarAgencia;
    EditText edtCadastrarConta;
    EditText edtCadastrarDigito;
    EditText edtCadastrarCPF;
    String nomeBanco;
    List<String> listBanco = new ArrayList<>();
    List<String> listIDBanco = new ArrayList<>();
    String stringSHA1;
    String idDoBanco;
    Button btnCadastroFinalizar;
    Boolean entraLa = false;
    String email;
    String emailPattern;
    String urlFoto;

    int requiredSize = 200;
    String imagemBase64;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    String base64Final = "";
    Dialog alertDialog_show;

    public static int RESULT_GALLERY = 0;

    private int RESULT_LOAD_IMG = 1;
    private int REQUEST_IMAGE_CAPTURE = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro2);

        sessao =  new AppShared(this);

        lnlCadastro2 = (LinearLayout) findViewById(R.id.lnlCadastro2);
        lnlCadastroTopo = (LinearLayout) findViewById(R.id.lnlCadastroTopo);
        lnlCadastroCorpo = (LinearLayout) findViewById(R.id.lnlCadastroCorpo);
        lnlCadastroFoto = (LinearLayout) findViewById(R.id.lnlCadastroFoto);
        txtVoltarCadastro = (TextView) findViewById(R.id.txtVoltarCadastro);
        imgCadastrarFoto = (CircleImageView) findViewById(R.id.imgCadastrarFoto);
        edtCadastrarNome = (EditText) findViewById(R.id.edtCadastrarNome);
        edtCadastrarEmail = (EditText) findViewById(R.id.edtCadastrarEmail);
        edtCadastrarSenha = (EditText) findViewById(R.id.edtCadastrarSenha);
        edtCadastrarTelefone = (EditText) findViewById(R.id.edtCadastrarTelefone);
        spnBanco2 = (Spinner) findViewById(R.id.spnBanco2);
        edtCadastrarAgencia = (EditText) findViewById(R.id.edtCadastrarAgencia);
        edtCadastrarConta = (EditText) findViewById(R.id.edtCadastrarConta);
        edtCadastrarDigito = (EditText) findViewById(R.id.edtCadastrarDigito);
        edtCadastrarCPF = (EditText) findViewById(R.id.edtCadastrarCPF);
        btnCadastroFinalizar = (Button) findViewById(R.id.btnCadastroFinalizar);

        lnlCadastro2.setOnClickListener(this);
        txtVoltarCadastro.setOnClickListener(this);
        edtCadastrarNome.setOnClickListener(this);
        lnlCadastroFoto.setOnClickListener(this);
        imgCadastrarFoto.setOnClickListener(this);
        btnCadastroFinalizar.setOnClickListener(this);

        System.out.println("valorSessao " + sessao.getFullNameFacebook());
        System.out.println("valorSessao " + sessao.getEmailFacebook());

        edtCadastrarNome.setText(sessao.getFullNameFacebook());
        edtCadastrarEmail.setText(sessao.getEmailFacebook());

        urlFoto = "https://graph.facebook.com/" + sessao.getIdFacebook() + "/picture?type=large";
        System.out.println("URL FOTO " + urlFoto);
        Picasso.with(this).load(urlFoto).into(imgCadastrarFoto);

        listarBancos();

        TextWatcher telefoneMask = Mask.insert("(##)#####-####", edtCadastrarTelefone);
        edtCadastrarTelefone.addTextChangedListener(telefoneMask);

        TextWatcher cpfMask = Mask.insert("###.###.###-##", edtCadastrarCPF);
        edtCadastrarCPF.addTextChangedListener(cpfMask);

        lnlCadastro2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        lnlCadastroTopo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        lnlCadastroCorpo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        lnlCadastroFoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);




    }

    @Override
    public void onClick(View view) {

        if (view == txtVoltarCadastro) {
            finish();
        } else if (view == imgCadastrarFoto) {
            chamaImagem();
        } else if (view == btnCadastroFinalizar) {

            email = edtCadastrarEmail.getText().toString().trim();
            emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

            if (edtCadastrarNome.getText().toString().equals("")) {
                Toast.makeText(this, "O campo nome está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarEmail.getText().toString().equals("")) {
                Toast.makeText(this, "O campo e-mail está vazio.", Toast.LENGTH_SHORT).show();
            } else if (!email.matches(emailPattern)) {
                Toast.makeText(getApplicationContext(), "Digite um e-mail válido.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarSenha.getText().toString().length() <= 7) {
                Toast.makeText(this, "A senha precisa ter no mínimo 8 caracteres", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarTelefone.getText().toString().equals("")) {
                Toast.makeText(this, "O campo celular está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarAgencia.getText().toString().equals("")) {
                Toast.makeText(this, "O campo agência está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarConta.getText().toString().equals("")) {
                Toast.makeText(this, "O campo conta está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarDigito.getText().toString().equals("")) {
                Toast.makeText(this, "O campo dígito está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarCPF.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CPF está vazio.", Toast.LENGTH_SHORT).show();
            } else if (base64Final.equals("")) {
                Toast.makeText(this, "Insira uma imagem para o seu perfil.", Toast.LENGTH_SHORT).show();
            } else {

                cadastrarUsuario();

            }
            // listarBancos();
        }

    }


    private void chamaImagem() {

        //MODAL VIEW TERMOS DE USO
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogbox = Cadastro2Activity.this.getLayoutInflater().inflate(R.layout.dialog_camera_sd, null);

        ImageView camera = dialogbox.findViewById(R.id.btn_camera);
        ImageView galeria = dialogbox.findViewById(R.id.btn_galeria);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
//        final TextView tv_titulo = dialogbox.findViewById(R.id.tv_titulo_historico_procedimento);

        builder.setView(dialogbox);
        alertDialog_show = builder.create();
        alertDialog_show.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog_show.show();


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(Cadastro2Activity.this.getBaseContext().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    alertDialog_show.dismiss();
                }

            }
        });

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, RESULT_LOAD_IMG);
                alertDialog_show.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_show.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        Log.w("Entrei", "OnResult");

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            Log.w("Entrei", "Galeria");
            try {

                final Uri imageUri = data.getData();
                final Bitmap selectedImage = decodeUri(imageUri);

                InputStream inputStream = this.getContentResolver().openInputStream(imageUri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                Log.w("Tamanhos da imagem", "Heigth = " + imageHeight + " : Width = " + imageWidth);
                if (imageHeight <= 150 || imageWidth <= 150) {
//                    ToolBox.ExibeMSG("Imagem muito pequena!", this);
                    Toast.makeText(this, "Imagem muito pequena, seleione outra.", Toast.LENGTH_SHORT).show();
                } else {
                    String base64 = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG);

                    System.gc();

                    GuardaImagem(selectedImage);
                    System.out.println("O BASE 64 DA GALERIA É " + base64);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(Cadastro2Activity.this, "Você não selecionou uma foto.", Toast.LENGTH_LONG).show();

            }


        } else if (resultCode == RESULT_OK && reqCode == REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String base64 = encodeToBase64(imageBitmap, Bitmap.CompressFormat.JPEG);

            System.gc();
            GuardaImagem(imageBitmap);

            System.out.println("O BASE 64 DA CAMERA É " + base64);
        } else {
            Toast.makeText(Cadastro2Activity.this, "Foto não Capturada.", Toast.LENGTH_LONG).show();
        }
        System.gc();


    }


    public Bitmap decodeUri(Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(Cadastro2Activity.this.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(Cadastro2Activity.this.getContentResolver().openInputStream(uri), null, o2);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }


    private void GuardaImagem(Bitmap bitmap) {

        String nova_imagem64 = Imagem_encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG);
        System.out.println("A STRING NOVA IMAGEM É " + nova_imagem64);
        base64Final = nova_imagem64;


        if (nova_imagem64.length() > 0) {
            Bitmap bitmap2 = Imagem_decodeBase64(nova_imagem64);
            imgCadastrarFoto.setImageBitmap(bitmap2);
        }

    }

    public static String Imagem_encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Função para Decodificar a imagem de String para Imagem Novamente
     */
    public static Bitmap Imagem_decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    public void listarBancos() {


        WebService ws = new WebService(
                getString(R.string.servidor),
                "listar_Bancos_Android",
                "POST",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);


                System.out.println("OIEEEEE");


                try {


                    JSONArray arrayInterno = objeto.getJSONArray("bancos");


                    for (int i = 0; i < arrayInterno.length(); i++) {

                        JSONObject objetoInterno = arrayInterno.getJSONObject(i);

                        String arrayIdBanco = objetoInterno.getString("idBanco");
                        String arrayBanco = objetoInterno.getString("nomeBanco");


                        System.out.println("ID DO BANCO É = " + arrayIdBanco);
                        System.out.println("O BANCO É = " + arrayBanco);

                        listBanco.add(arrayBanco);
                        listIDBanco.add(arrayIdBanco);

                        ArrayAdapter arrayAdapterNomeBanco = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listBanco);
                        spnBanco2.setAdapter(arrayAdapterNomeBanco);

                        spnBanco2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                idDoBanco = listIDBanco.get(i);
                                System.out.println("O ID DO BANCO É == " + idDoBanco);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public void cadastrarUsuario() {

        System.out.println("CADASTRAR USUÁRIO");

        try {
            stringSHA1 = StringEncryption.SHA1(edtCadastrarSenha.getText().toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Map<String, String> parametros = new HashMap<>();

        entraLa = true;

        parametros.put("nomeUsuario", edtCadastrarNome.getText().toString());
        parametros.put("nomeUsuarioVisualizacao", edtCadastrarNome.getText().toString());
        parametros.put("senhaUsuario", stringSHA1);
        parametros.put("emailUsuario", edtCadastrarEmail.getText().toString());
        parametros.put("telefoneUsuario", edtCadastrarTelefone.getText().toString());
        parametros.put("bancoUsuario", idDoBanco);
        parametros.put("agenciaUsuario", edtCadastrarAgencia.getText().toString());
        parametros.put("contaUsuario", edtCadastrarConta.getText().toString());
        parametros.put("digitoUsuario", edtCadastrarDigito.getText().toString());
        parametros.put("cpfUsuario", edtCadastrarCPF.getText().toString());
        parametros.put("imagemUsuario", base64Final);
        parametros.put("id_facebook", sessao.getIdFacebook());
        parametros.put("link_facebook", "");


        WebService ws = new WebService(
                getString(R.string.servidor),
                "novo_Usuario_Android",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("success");
                    String message = objeto.getString("message");


                    System.out.println("STATUS DO CADASTRO PASSAGEIRO É = " + status);


                    if (status == 1) {

                        Toast.makeText(Cadastro2Activity.this, "Seu cadastro foi efetuado com sucesso.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Cadastro2Activity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(Cadastro2Activity.this, message, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

}
