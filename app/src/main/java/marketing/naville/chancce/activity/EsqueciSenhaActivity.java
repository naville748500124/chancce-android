package marketing.naville.chancce.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import marketing.naville.chancce.R;
import marketing.naville.chancce.webservice.ChecarUsuarioExistente;

public class EsqueciSenhaActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtEsqueciSenha;
    private Button btnRecuperarSenha;
    private TextView txtVoltarBranco;
    private LinearLayout lnlEsqueci1;
    private LinearLayout lnlEsqueci2;
    private LinearLayout lnlEsqueci3;
    private LinearLayout lnlEsqueci4;
    private LinearLayout lnlEsqueci5;


    //requer no webservice
    RequestQueue requisicao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueci_senha);

        //requer no webservice
        requisicao = Volley.newRequestQueue(this);

        edtEsqueciSenha = (EditText) findViewById(R.id.edtEsqueciSenha);
        btnRecuperarSenha = (Button) findViewById(R.id.btnRecuperarSenha);
        txtVoltarBranco = (TextView) findViewById(R.id.txtVoltarBranco);
        lnlEsqueci1 = (LinearLayout) findViewById(R.id.lnlEsqueci1);
        lnlEsqueci2 = (LinearLayout) findViewById(R.id.lnlEsqueci2);
        lnlEsqueci3 = (LinearLayout) findViewById(R.id.lnlEsqueci3);
        lnlEsqueci4 = (LinearLayout) findViewById(R.id.lnlEsqueci4);
        lnlEsqueci5 = (LinearLayout) findViewById(R.id.lnlEsqueci5);


        txtVoltarBranco.setOnClickListener(this);
        btnRecuperarSenha.setOnClickListener(this);
        lnlEsqueci1.setOnClickListener(this);
        lnlEsqueci2.setOnClickListener(this);
        lnlEsqueci3.setOnClickListener(this);
        lnlEsqueci4.setOnClickListener(this);
        lnlEsqueci5.setOnClickListener(this);

        //Tirar o teclado quando clicar fora
        lnlEsqueci1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        lnlEsqueci2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        lnlEsqueci3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        lnlEsqueci4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        lnlEsqueci5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == txtVoltarBranco) {

            startActivity(new Intent(EsqueciSenhaActivity.this, LoginActivity.class));
            finish();


        }
        if (view == btnRecuperarSenha) {
            final String email = edtEsqueciSenha.getText().toString();

            if (email.equals("")) {
                Toast.makeText(this, "Digite um e-mail válido.!", Toast.LENGTH_SHORT).show();
            } else {

                chamarWebServiceRestaurar();

//                String url = getString(R.string.servidor) + "recuperarSenha";
//
//                requisicao.start();
//                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                        new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//
//                                try {
//                                    JSONObject jsonObject = new JSONObject(response);
//
//                                    if (Integer.parseInt(jsonObject.get("status").toString()) == 0) {
//                                        Toast.makeText(EsqueciSenhaActivity.this, "E-mail inválido.", Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        Toast.makeText(EsqueciSenhaActivity.this, "Senha enviada com sucesso para o seu e-mail.", Toast.LENGTH_SHORT).show();
//                                    }
//
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//
//                        }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(EsqueciSenhaActivity.this, "Erro  ao Conectar com o Banco de Dados. Tente novamente", Toast.LENGTH_SHORT).show();
//                    }
//                }) {
//                    @Override
//                    public Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<>();
//
//                        params.put("emailUsuario", email);
//
//                        return params;
//                    }
//                };
//
//                requisicao.add(stringRequest);
//
//            }
//
            }
        }
    }

    public void chamarWebServiceRestaurar() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("emailUsuario", edtEsqueciSenha.getText().toString());

        WebService ws = new WebService(
                getString(R.string.servidor),
                "recuperarSenha",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");


                    System.out.println("Resultado = " + status);


                    if (status == 0) {

                        Toast.makeText(getApplicationContext(), "Esse e-mail não possui conta no Chancce.", Toast.LENGTH_SHORT).show();
                    } else {
                        finish();
                        Toast.makeText(EsqueciSenhaActivity.this, "Uma nova senha foi enviada para o seu e-mail.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    System.out.println(" Esqueci senha Catch : " + e.getMessage());
                }
            }
        });


    }
}
