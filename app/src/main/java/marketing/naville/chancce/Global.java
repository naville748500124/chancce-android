package marketing.naville.chancce;

import java.util.ArrayList;

import marketing.naville.chancce.entidades.Oportunidade;

public class Global {
    public static int idUsuario = 0;
    public static String nomeUsuario = "";
    public static String nomeUsuarioVisualizacao = "";
    public static String emailUsuario = "";
    public static String senhaUsuario = "";
    public static String telefoneUsuario = "";
    public static int bancoUsuario = 1;
    public static String agenciaUsuario = "";
    public static String contaUsuario = "";
    public static String cpfUsuario = "";
    public static String imagem = "";
    public static String imagemUsurario = "";
    public static String digitoUsuario = "";



    public static String[] urls;
    public static ArrayList<Oportunidade> listaOportunidades = new ArrayList<>();

    public static int validaOportunidade = 0;
}
