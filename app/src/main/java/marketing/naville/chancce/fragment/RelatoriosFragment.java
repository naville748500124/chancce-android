package marketing.naville.chancce.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import marketing.naville.chancce.adapter.CustomAdapterRelatorios;
import marketing.naville.chancce.R;

public class RelatoriosFragment extends Fragment {
    private ListView lstRelatorios;
    private Spinner spnDiaDe;
    private Spinner spnMesDe;
    private Spinner spnAnoDe;
    private Spinner spnDiaAte;
    private Spinner spnMesAte;
    private Spinner spnAnoAte;

    private ArrayAdapter<Integer> adpDiaDe;
    private ArrayAdapter<Integer> adpMesDe;
    private ArrayAdapter<Integer> adpAnoDe;
    private ArrayAdapter<Integer> adpDiaAte;
    private ArrayAdapter<Integer> adpMesAte;
    private ArrayAdapter<Integer> adpAnoAte;

    public static String [] oportunidades = {"Oportunidade 1", "Oportunidade 2", "Oportunidade 3"};
    public static String [] datas = {"24 de Abril de 1998", "3 de Janeiro de 1983", "28 de Maio de 2007"};

    public RelatoriosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_relatorios, container, false);

        spnDiaDe = (Spinner) view.findViewById(R.id.spnDiaDe);
        spnMesDe = (Spinner) view.findViewById(R.id.spnMesDe);
        spnAnoDe = (Spinner) view.findViewById(R.id.spnAnoDe);

        spnDiaAte = (Spinner) view.findViewById(R.id.spnDiaAte);
        spnMesAte = (Spinner) view.findViewById(R.id.spnMesAte);
        spnAnoAte = (Spinner) view.findViewById(R.id.spnAnoAte);

        adpDiaDe = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpDiaDe.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        adpMesDe = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpMesDe.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        adpAnoDe = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpAnoDe.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        adpDiaAte = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpDiaAte.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        adpMesAte = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpMesAte.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        adpAnoAte = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adpAnoAte.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        //DIA
        for(int i = 1; i <= 31; i++) {
            adpDiaDe.add(i);
            adpDiaAte.add(i);
        }

        //MES
        for(int i = 1; i <= 12; i++) {
            adpMesDe.add(i);
            adpMesAte.add(i);
        }

        //ANO
        for(int i = 2000; i <= 2017; i++) {
            adpAnoDe.add(i);
            adpAnoAte.add(i);
        }

        spnDiaDe.setAdapter(adpDiaDe);
        spnMesDe.setAdapter(adpMesDe);
        spnAnoDe.setAdapter(adpAnoDe);

        spnDiaAte.setAdapter(adpDiaAte);
        spnMesAte.setAdapter(adpMesAte);
        spnAnoAte.setAdapter(adpAnoAte);

        lstRelatorios = (ListView) view.findViewById(R.id.lstRelatorios);
        lstRelatorios.setAdapter(new CustomAdapterRelatorios(getActivity(), oportunidades, datas));

        MenuInferiorFragment menuInferiorFragment = new MenuInferiorFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragmentMenuInferior, menuInferiorFragment, "menuInferiorFragment");
        fragmentTransaction.commit();

        return view;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
