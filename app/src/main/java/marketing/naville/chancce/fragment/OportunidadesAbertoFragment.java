package marketing.naville.chancce.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.CompararActivity;
import marketing.naville.chancce.R;
import marketing.naville.chancce.adapter.CustomAdapterOportunidadesAberto;


public class OportunidadesAbertoFragment extends Fragment {


    ListView lstOportunidadesAberto;

    public String[] oportunidades;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    int id_bloco;
    CustomAdapterOportunidadesAberto adapterOportunidadesAberto;

    ProgressDialog dialog;




    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();


    }

    private void carregarAlert() {

        sessao.setBadge(0);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Você não tem oportunidades em aberto no momento.")
                .setCancelable(false)

                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public OportunidadesAbertoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("VOLTANDO PRO ON RESUME");
        carregarTabela();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_oportunidades_aberto, container, false);


        lstOportunidadesAberto = (ListView) view.findViewById(R.id.lstOportunidadesAberto);

        sessao = new AppShared(getContext());

        requisicao = Volley.newRequestQueue(view.getContext());



        return view;
    }

    public void carregarTabela() {
        String URL = getString(R.string.servidor) + "listar_Blocos_Usuario";


        comecarLoad();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    jsonarray = new JSONArray(response);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        id_bloco = jsonobject.getInt("id_bloco");


                        int tempo_restante = jsonobject.getInt("tempo_restante");
                        String data_bloco = jsonobject.getString("data_bloco");
                        int badge_blocos = jsonobject.getInt("badge_blocos");


                        System.out.println("O ID DO BLOCO É: " + id_bloco);
                        System.out.println("O TEMPO RESTANTE DO BLOCO É: " + tempo_restante);
                        System.out.println("A DATA DO BLOCO É: " + data_bloco);
                        System.out.println("O VALOR DOS BADGES DO BLOCO É: " + badge_blocos);

                        sessao.setBadge(badge_blocos);


                    }


                    lstOportunidadesAberto.setAdapter(new CustomAdapterOportunidadesAberto(getActivity(), jsonarray));

                    lstOportunidadesAberto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            sessao.setIdBloco(String.valueOf(id_bloco));
                            Intent intent = new Intent(getContext(), CompararActivity.class);
                            getContext().startActivity(intent);


                        }
                    });


                    if (jsonarray.length() == 0) {
                        carregarAlert();
                    }


                    dialog.dismiss();

                } catch (JSONException e) {
                    dialog.dismiss();


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                dialog.dismiss();

                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("id_usuario", sessao.getId() + "");


                return parameters;
            }

        };

        requisicao.add(stringRequest);
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
