package marketing.naville.chancce.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.R;
import marketing.naville.chancce.adapter.CustomAdapterNovasOportunidades;
import marketing.naville.chancce.entidades.Oportunidade;

public class NovasOportunidadesFragment extends Fragment {
    ListView lstNovasOportunidades;

    private ArrayList<Oportunidade> listaOportunidades = new ArrayList<>();

    public String[] oportunidades;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    ProgressDialog dialog;


    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO NOVAS OPORTUNIDADES");


    }

    private void carregarAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("No momento não há blocos disponíveis.")
                .setCancelable(false)

                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public NovasOportunidadesFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("VOLTANDO PRO ON RESUME");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_novas_oportunidades, container, false);

        lstNovasOportunidades = view.findViewById(R.id.lstNovasOportunidades);
        requisicao = Volley.newRequestQueue(view.getContext());

        carregarTabela();


        // baixarOportunidades();
        final Oportunidade oportunidade = new Oportunidade();
        listaOportunidades.add(oportunidade);


        sessao = new AppShared(view.getContext());

        System.out.println("O ID DO USUARIO É: " + sessao.getId());
        System.out.println("ESSE É O ID DO USUARIO: " + GlobalConfiguracoes.idFacebook);

        final Global global = new Global();


        if (global.validaOportunidade == 1) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
            System.out.println(global.validaOportunidade);
            global.validaOportunidade = 0;


        } else {
            System.out.println("Chegou no Else");
        }


        System.out.println(global.validaOportunidade);


        return view;
    }

    public interface OnFragmentInteractionListener {

    }

    public void carregarTabela() {
        String URL = getString(R.string.servidor) + "listar_Blocos";

        comecarLoad();
        System.out.println("COMECOU O LOAD");


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    jsonarray = new JSONArray(response);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        int id_bloco = jsonobject.getInt("id_bloco");
                        double valor = jsonobject.getDouble("valor");
                        String data_bloco = jsonobject.getString("data_bloco");


                        System.out.println("O ID DO BLOCO É: " + id_bloco);
                        System.out.println("O VALOR DO BLOCO É: " + valor);
                        System.out.println("A DATA DO BLOCO É: " + data_bloco);


                    }


                        lstNovasOportunidades.setAdapter(new CustomAdapterNovasOportunidades(getActivity(), jsonarray));

                    dialog.dismiss();


                    if (jsonarray.length() == 0) {
                        carregarAlert();
                    }


                } catch (JSONException e) {

                    dialog.dismiss();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("id_usuario", sessao.getId() + "");


                return parameters;
            }

        };

        requisicao.add(stringRequest);
    }


    public void listarOportunidades() {


        lstNovasOportunidades.setAdapter(new CustomAdapterNovasOportunidades(getActivity(), jsonarray));
    }


    private Date dataOntem() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(dataOntem());
    }

    private String getDString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(dataOntem());
    }
}
