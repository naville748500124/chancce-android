package marketing.naville.chancce.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.Mask;
import marketing.naville.chancce.R;
import marketing.naville.chancce.RestaurarActivity;
import marketing.naville.chancce.activity.CadastroActivity;
import marketing.naville.chancce.activity.DrawerActivity;
import marketing.naville.chancce.adapter.CustomAdapterGanhos;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class PerfilFragment extends Fragment implements View.OnClickListener {
    private EditText edtNome;
    private EditText edtEmail;
    private EditText edtTelefone;
    private Spinner spnBanco;
    private EditText edtAgencia;
    private EditText edtConta;
    private EditText edtCpf;
    private EditText edtContaDigito;
    private Button btnAlterarSenha;

    ProgressDialog dialog;


    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO CONFIGURACOES");


    }


    ArrayList<String> spnArray;

    String nomeBanco;


    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;

    public PerfilFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, null);

        edtNome = view.findViewById(R.id.edtNome);
        edtEmail = view.findViewById(R.id.edtEmail);
        EditText edtSenha = view.findViewById(R.id.edtSenha);
        edtTelefone = view.findViewById(R.id.edtTelefone);
        spnBanco = view.findViewById(R.id.spnBanco);
        edtConta = view.findViewById(R.id.edtConta);
        edtAgencia = view.findViewById(R.id.edtAgencia);
        edtCpf = view.findViewById(R.id.edtCpf);
        edtContaDigito = view.findViewById(R.id.edtContaDigito);
        FrameLayout lnlConfiguracoes1 = view.findViewById(R.id.lnlConfiguracoes1);
        LinearLayout lnlConfiguracoes2 = view.findViewById(R.id.lnlConfiguracoes2);
        LinearLayout lnlConfiguracoes3 = view.findViewById(R.id.lnlConfiguracoes3);
        LinearLayout lnlConfiguracoes4 = view.findViewById(R.id.lnlConfiguracoes4);
        btnAlterarSenha = view.findViewById(R.id.btnAlterarSenha);

        requisicao = Volley.newRequestQueue(view.getContext());


        lnlConfiguracoes1.setOnClickListener(this);
        edtCpf.setOnClickListener(this);
        btnAlterarSenha.setOnClickListener(this);

        sessao = new AppShared(view.getContext());
        spnArray = new ArrayList<String>();


        TextWatcher telefoneMask = Mask.insert("(##)####-#####", edtTelefone);
        edtTelefone.addTextChangedListener(telefoneMask);

        TextWatcher cpfMask = Mask.insert("###.###.###-##", edtCpf);
        edtCpf.addTextChangedListener(cpfMask);

        if (getActivity() instanceof DrawerActivity) {
            boolean configuracoes = true;

        }


        edtNome.setText(sessao.getUsuario() + "");
        edtEmail.setText(sessao.getEmail() + "");
        edtTelefone.setText(sessao.getTelefone() + "");
        edtAgencia.setText(sessao.getAgencia() + "");
        edtConta.setText(sessao.getConta() + "");
        edtContaDigito.setText(sessao.getDigito() + "");
        edtCpf.setText(sessao.getCpf() + "");


        //Tirar o teclado quando clicar fora
        lnlConfiguracoes1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlConfiguracoes2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlConfiguracoes3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlConfiguracoes4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

//        edtNome.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//
//                GlobalConfiguracoes.nomeUsuario = edtNome.getText().toString();
//                GlobalConfiguracoes.nomeUsuarioVisualizacao = edtNome.getText().toString().toLowerCase();
//
//
//                Global.nomeUsuario = edtNome.getText().toString();
//                Global.nomeUsuarioVisualizacao = edtNome.getText().toString().toLowerCase();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//
//        edtEmail.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                System.out.println("O EMAIL DIGITADO FOI O: " + edtEmail.getText().toString());
//
//                GlobalConfiguracoes.emailUsuario = edtEmail.getText().toString();
//
//
//                Global.emailUsuario = edtEmail.getText().toString();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        edtConta.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                GlobalConfiguracoes.contaUsuario = edtConta.getText().toString();
//
//                Global.contaUsuario = edtConta.getText().toString();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//
//        edtContaDigito.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                GlobalConfiguracoes.digitoUsuario = edtContaDigito.getText().toString();
//
//                Global.digitoUsuario = edtContaDigito.getText().toString();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        edtCpf.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                GlobalConfiguracoes.cpfUsuario = edtCpf.getText().toString();
//
//                Global.cpfUsuario = edtCpf.getText().toString();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        edtAgencia.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                GlobalConfiguracoes.agenciaUsuario = edtAgencia.getText().toString();
//
//                Global.agenciaUsuario = edtAgencia.getText().toString();
//
//            }
//
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });


        GlobalConfiguracoes.nomeUsuario = edtNome.getText().toString();
        GlobalConfiguracoes.nomeUsuarioVisualizacao = edtNome.getText().toString();
        GlobalConfiguracoes.emailUsuario = edtEmail.getText().toString();
        GlobalConfiguracoes.telefoneUsuario = edtTelefone.getText().toString();
        GlobalConfiguracoes.agenciaUsuario = edtAgencia.getText().toString();
        GlobalConfiguracoes.contaUsuario = edtConta.getText().toString();
        GlobalConfiguracoes.digitoUsuario = edtContaDigito.getText().toString();
        GlobalConfiguracoes.cpfUsuario = edtCpf.getText().toString();

        spnBanco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                GlobalConfiguracoes.bancoUsuario = (int) spnBanco.getSelectedItem();

                Global.bancoUsuario = (int) spnBanco.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//
//        edtTelefone.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                GlobalConfiguracoes.telefoneUsuario = edtTelefone.getText().toString();
//
//                Global.telefoneUsuario = edtTelefone.getText().toString();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });

        String URL = getString(R.string.servidor) + "listar_Bancos";

        comecarLoad();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                JSONObject objeto = null;

                try {

                    jsonarray = new JSONArray(response);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        int idBanco = jsonobject.getInt("idBanco");
                        nomeBanco = jsonobject.getString("nomeBanco");


                        System.out.println("O ID DO BLOCO É: " + idBanco);
                        System.out.println("A DATA DO BLOCO É: " + nomeBanco);


                        spnArray.add(nomeBanco);

                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spnArray);
                    spnBanco.setAdapter(arrayAdapter);
                    spnBanco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            System.out.println("O BANCO SELECIONADO FOI :" + i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    dialog.dismiss();


                } catch (JSONException e) {
                    dialog.dismiss();


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
//


                return parameters;
            }


        };

        requisicao.add(stringRequest);


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return view;
    }

    @Override
    public void onClick(View view) {

        if (view == btnAlterarSenha) {

            Intent intent = new Intent(getActivity(), RestaurarActivity.class);
            startActivity(intent);


        }

    }


}
