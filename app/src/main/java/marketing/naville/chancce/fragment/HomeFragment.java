package marketing.naville.chancce.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.R;
import marketing.naville.chancce.adapter.CustomAdapterHome;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private ImageView imgSetaNovasOportunidades;
    private ImageView imgSetaOportunidadesAberto;
    private ImageView imgSetaMeusGanhos;
    private ImageView imgSetaRelatorio;
    private TextView txtNovasOportunidades;
    private TextView txtOportunidadesAberto;
    private TextView txtMeusGanhos;
    private TextView txtRelatorios;
    private ListView lstMeusGanhos;
    private TextView txtNome2;
    private TextView txtTotalDesconto;
    private TextView txtTotalTotal;
    CircleImageView imgoFotoHome;

    double totalDescontos = 0;
    double totalTotal = 0;

    String totalDescontosString;
    String totalTotalString;

    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;
    ProgressDialog dialog;


    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO HOME");


    }


    public static String[] oportunidades = {"Oportunidade 1", "Oportunidade 2", "Oportunidade 3"};
    public static String[] datas = {"24 de Abril de 1998", "3 de Janeiro de 1983", "28 de Maio de 2007"};
    public static double[] valores = {24.95, 32.99, 100};
    public static double[] descontos = {0, 22, 32.12};

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        lstMeusGanhos = view.findViewById(R.id.lstMeusGanhos);

        sessao = new AppShared(this.getContext());
        requisicao = Volley.newRequestQueue(view.getContext());


        GlobalConfiguracoes.nomeUsuarioVisualizacao = Global.nomeUsuarioVisualizacao;

        txtNome2 = view.findViewById(R.id.txtNome2);
        txtNome2.setText(sessao.getUsuario());

        txtTotalDesconto = view.findViewById(R.id.txtTotalDesconto);
        txtTotalTotal = view.findViewById(R.id.txtTotalTotal);


        imgoFotoHome = view.findViewById(R.id.imgoFotoHome);


        imgoFotoHome.setOnClickListener(this);


        sessao.getUrlImg();

        System.out.println("A URL DA IMAGEM É: " + sessao.getUrlImg());

        String imagemPerfil = getString(R.string.servidorImagem) + sessao.getUrlImg();
        System.out.println("A IMAGEM DO PERFIL É = " + imagemPerfil);

        Picasso.with(getActivity()).load(imagemPerfil).into(imgoFotoHome);


        String URL = getString(R.string.servidor) + "bloco_pos_auditado";

        comecarLoad();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                JSONObject objeto = null;

                try {


                    jsonarray = new JSONArray(response);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        int id_bloco = jsonobject.getInt("id_bloco");
                        String data_bloco = jsonobject.getString("data_bloco");
                        int qtd_canhotos = jsonobject.getInt("qtd_canhotos");
                        double valor = jsonobject.getDouble("valor");
                        int auditados = jsonobject.getInt("auditados");
                        int pos_auditados = jsonobject.getInt("pos_auditados");
                        double valor_pos_auditado = jsonobject.getDouble("valor_pos_auditado");
                        double valor_desconto = jsonobject.getDouble("valor_desconto");

                        totalDescontos += valor_desconto;
                        totalTotal += valor_pos_auditado;

                        totalDescontosString = String.valueOf(totalDescontos).replace(".", ",") + "0";
                        totalTotalString = String.valueOf(totalTotal).replace(".", ",");


                        System.out.println("O ID DO BLOCO É: " + id_bloco);
                        System.out.println("A DATA DO BLOCO É: " + data_bloco);
                        System.out.println("A QUANTIDADE DE CANHOTOS É: " + qtd_canhotos);
                        System.out.println("O VALOR DO BLOCO É: " + valor);
                        System.out.println("OS AUDITADOS SÃO: " + auditados);
                        System.out.println("OS PÓS AUDITADOS SÃO: " + pos_auditados);
                        System.out.println("OS VALORES PÓS AUDITADOS SÃO: " + valor_pos_auditado);
                        System.out.println("OS VALORES DE DESCONTO SÃO: " + valor_desconto);


                    }

                    lstMeusGanhos.setAdapter(new CustomAdapterHome(getActivity(), jsonarray));
                    txtTotalDesconto.setText(totalDescontosString);
                    txtTotalTotal.setText(totalTotalString);


                    if (jsonarray.length() == 0) {
                        carregarAlert();
                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    dialog.dismiss();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parameters = new HashMap<String, String>();

                parameters.put("id_usuario", sessao.getId() + "");


                return parameters;
            }

        };

        requisicao.add(stringRequest);


        return view;
    }

    @Override
    public void onStart() {

        String imagemPerfil = getString(R.string.servidorImagem) + sessao.getUrlImg();
        System.out.println("A IMAGEM DO PERFIL É = " + imagemPerfil);
        System.out.println("Entrou no nStart");

        Picasso.with(getActivity()).load(imagemPerfil).into(imgoFotoHome);

        super.onStart();
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = null;

        if (view == imgSetaNovasOportunidades || view == txtNovasOportunidades) {
            fragment = new NovasOportunidadesFragment();
        } else if (view == imgSetaOportunidadesAberto || view == txtOportunidadesAberto) {
            fragment = new OportunidadesAbertoFragment();
        } else if (view == imgSetaMeusGanhos || view == txtMeusGanhos) {
            fragment = new GanhosFragment();
        } else if (view == imgSetaRelatorio || view == txtRelatorios) {
            fragment = new RelatoriosFragment();
        }

        if(fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.flContent, fragment);
            fragmentTransaction.commit();
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    @SuppressLint("SetTextI18n")
    private void carregarAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Você não tem blocos pós-auditados no momento no momento.")
                .setCancelable(false)

                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        txtTotalDesconto.setText("0,00");
        txtTotalTotal.setText("0,00");

    }
}
