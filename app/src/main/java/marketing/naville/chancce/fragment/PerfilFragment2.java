package marketing.naville.chancce.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.Mask;
import marketing.naville.chancce.R;
import marketing.naville.chancce.TermosActivity;
import marketing.naville.chancce.activity.DrawerActivity;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class PerfilFragment2 extends Fragment implements View.OnClickListener {
    private EditText edtNome;
    private EditText edtEmail;
    private EditText edtSenha;
    private EditText edtTelefone;
    private Spinner spnBanco;
    private EditText edtAgencia;
    private EditText edtConta;
    private EditText edtDigito;
    private EditText edtCpf;
    private TextView txtTermos;
    int vIdBanco = 0;
    FrameLayout frmCadastro1;

    int idBanco;

    ProgressDialog dialog;

    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO CONFIGURACOES2");


    }

    String nomeBanco;
    ArrayList<String> spnArray;
    ArrayList<Integer> spnArrayId;


    JSONArray jsonarray;
    RequestQueue requisicao;


    private boolean configuracoes = false;
    private boolean senhaConfirmada = false;
    private String confirmarSenha = "";


    public PerfilFragment2() {
        // Required empty public constructor
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil2, null);

//        edtNome = view.findViewById(R.id.edtNome);
//        edtEmail = view.findViewById(R.id.edtEmail);
//        edtSenha = view.findViewById(R.id.edtSenha);
//        edtTelefone = view.findViewById(R.id.edtTelefone);
//        spnBanco = view.findViewById(R.id.spnBanco2);
//        edtConta = view.findViewById(R.id.edtConta);
//        edtDigito = view.findViewById(R.id.edtDigito);
//        edtAgencia = view.findViewById(R.id.edtAgencia);
//        edtCpf = view.findViewById(R.id.edtCpf);
//        txtTermos = view.findViewById(R.id.txtTermos);
//        requisicao = Volley.newRequestQueue(view.getContext());
//        spnArray = new ArrayList<>();
//        spnArrayId = new ArrayList<>();
//        edtSenha.setTransformationMethod(new PasswordTransformationMethod());
//        frmCadastro1 = view.findViewById(R.id.frmCadastro1);

        if (!Objects.equals(GlobalConfiguracoes.nomeUsuario, "")) {
            edtNome.setText(GlobalConfiguracoes.nomeUsuario);
        }

        if (!Objects.equals(GlobalConfiguracoes.emailUsuario, "")) {
            edtEmail.setText(GlobalConfiguracoes.emailUsuario);
        }


        txtTermos.setOnClickListener(this);
        TextWatcher telefoneMask = Mask.insert("(##)####-#####", edtTelefone);
        edtTelefone.addTextChangedListener(telefoneMask);
        TextWatcher cpfMask = Mask.insert("###.###.###-##", edtCpf);
        edtCpf.addTextChangedListener(cpfMask);

        frmCadastro1.setOnClickListener(this);


        if (getActivity() instanceof DrawerActivity) {
            configuracoes = true;
            edtSenha.setFocusable(false);
            edtSenha.setFocusableInTouchMode(false);
            edtSenha.setClickable(false);
        }

        edtNome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.nomeUsuarioVisualizacao = edtNome.getText().toString();
                } else {
                    Global.nomeUsuarioVisualizacao = edtNome.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.emailUsuario = edtEmail.getText().toString();
                } else {
                    Global.emailUsuario = edtEmail.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtConta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.contaUsuario = edtConta.getText().toString();
                } else {
                    Global.contaUsuario = edtConta.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edtDigito.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.digitoUsuario = edtDigito.getText().toString();
                } else {
                    Global.digitoUsuario = edtDigito.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edtCpf.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.cpfUsuario = edtCpf.getText().toString();
                } else {
                    Global.cpfUsuario = edtCpf.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edtAgencia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.agenciaUsuario = edtAgencia.getText().toString();
                } else {
                    Global.agenciaUsuario = edtAgencia.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

//        spnBanco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (configuracoes) {
//                    GlobalConfiguracoes.bancoUsuario = spnBanco.getSelectedItem().toString();
//                } else {
//                    Global.bancoUsuario = spnBanco.getSelectedItem().toString();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });


        edtSenha.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.senhaUsuario = edtSenha.getText().toString();
                } else {
                    Global.senhaUsuario = edtSenha.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        edtTelefone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (configuracoes) {
                    GlobalConfiguracoes.telefoneUsuario = edtTelefone.getText().toString();
                } else {
                    Global.telefoneUsuario = edtTelefone.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edtSenha.setOnClickListener(this);
//
//        String URL = getString(R.string.servidor) + "listar_Bancos";
//        comecarLoad();
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//
//                try {
//
//                    jsonarray = new JSONArray(response);
//                    for (int i = 0; i < jsonarray.length(); i++) {
//                        JSONObject jsonobject = jsonarray.getJSONObject(i);
//                        idBanco = jsonobject.getInt("idBanco");
//                        nomeBanco = jsonobject.getString("nomeBanco");
//
//
//                        System.out.println("O ID DO BLOCO É: " + idBanco);
//                        System.out.println("A DATA DO BLOCO É: " + nomeBanco);
//
//
//                        spnArray.add(nomeBanco);
//                        spnArrayId.add(idBanco);
//
//                    }
//
//                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spnArray);
//                    spnBanco.setAdapter(arrayAdapter);
//                    spnBanco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                            System.out.println("O BANCO SELECIONADO FOI :" + spnArray.get(i) + " indice " + spnArrayId.get(i));
//                            vIdBanco = spnArrayId.get(i);
//                            Global.bancoUsuario = vIdBanco;
//
////                                    spnBanco.getSelectedItem().toString();
//
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                        }
//                    });
//
//                    dialog.dismiss();
//
//
//                } catch (JSONException e) {
//
//                    dialog.dismiss();
//
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                dialog.dismiss();
//                System.out.println(error.getMessage());
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> parameters = new HashMap<String, String>();
//
//
//                return parameters;
//            }
//
//
//        };
//
//        requisicao.add(stringRequest);
//
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//
        return view;
    }


    @Override
    public void onClick(View view) {


        if (view == txtTermos) {

            Intent intent = new Intent(getActivity(), TermosActivity.class);
            startActivity(intent);


        }


    }
}
