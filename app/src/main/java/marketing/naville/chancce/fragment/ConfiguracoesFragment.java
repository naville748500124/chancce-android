package marketing.naville.chancce.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.Global;
import marketing.naville.chancce.GlobalConfiguracoes;
import marketing.naville.chancce.OnBackPressedListener;
import marketing.naville.chancce.R;
import marketing.naville.chancce.StringEncryption;
import marketing.naville.chancce.activity.CadastroActivity;
import marketing.naville.chancce.entidades.Usuario;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;

public class ConfiguracoesFragment extends Fragment implements View.OnClickListener, OnBackPressedListener {
    private TextView txtNome;
    private Button btnFinalizar;
    private int tela;
    private FrameLayout lnlConfiguracoes5;
    private LinearLayout lnlConfiguracoes6;
    private LinearLayout lnlconfiguracoes7;
    private LinearLayout lnlConfiguracoes8;
    JSONObject jsonobjeto;
    AppShared sessao;
    RequestQueue requisicao;
    private ImageView imgFoto;
    String base64Atual;

    private static int PICK_IMAGE = 1;
    private static int RESULT_LOAD_IMAGE = 1;
    int requiredSize = 200;
    String imagemBase64;


    String base64Final;

    Dialog alertDialog_show;

    public static int RESULT_GALLERY = 0;

    private int RESULT_LOAD_IMG = 1;
    private int REQUEST_IMAGE_CAPTURE = 2;


    public ConfiguracoesFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_configuracoes, container, false);


        btnFinalizar = view.findViewById(R.id.btnFinalizar);
        imgFoto = view.findViewById(R.id.btnFoto2);
        lnlConfiguracoes5 = view.findViewById(R.id.lnlConfiguracoes5);
        lnlConfiguracoes6 = view.findViewById(R.id.lnlConfiguracoes6);
        lnlconfiguracoes7 = view.findViewById(R.id.lnlConfiguracoes7);
        lnlConfiguracoes8 = view.findViewById(R.id.lnlConfiguracoes8);


            requisicao = Volley.newRequestQueue(getContext());
        sessao = new AppShared(this.getContext());


        GlobalConfiguracoes.nomeUsuarioVisualizacao = Global.nomeUsuarioVisualizacao;
        GlobalConfiguracoes.emailUsuario = Global.emailUsuario;
        GlobalConfiguracoes.senhaUsuario = Global.senhaUsuario;
        GlobalConfiguracoes.telefoneUsuario = Global.telefoneUsuario;
        GlobalConfiguracoes.bancoUsuario = Global.bancoUsuario;
        GlobalConfiguracoes.agenciaUsuario = Global.agenciaUsuario;
        GlobalConfiguracoes.contaUsuario = Global.contaUsuario;
        GlobalConfiguracoes.cpfUsuario = Global.cpfUsuario;


        sessao.getUrlImg();

        System.out.println("A URL DA IMAGEM É: " + sessao.getUrlImg());

        String imagemPerfil = getString(R.string.servidorImagem) + sessao.getUrlImg();

        System.out.println(imagemPerfil);

        Picasso.with(getActivity()).load(imagemPerfil).into(imgFoto);

        Bitmap bitmapImagem = loadBitmap(imagemPerfil);
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.(bitmapImagem, options);
//        int imageHeight = options.outHeight;
//        int imageWidth = options.outWidth;


        String tamanho = String.valueOf(bitmapImagem);

        if (!tamanho.equals("null")){

            base64Atual = Imagem_encodeToBase64(bitmapImagem, Bitmap.CompressFormat.PNG);

        }



        btnFinalizar.setOnClickListener(this);
        imgFoto.setOnClickListener(this);
        lnlConfiguracoes5.setOnClickListener(this);
        lnlConfiguracoes6.setOnClickListener(this);
        lnlconfiguracoes7.setOnClickListener(this);
        lnlConfiguracoes8.setOnClickListener(this);


        //Tirar o teclado quando clicar fora
        lnlConfiguracoes5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlConfiguracoes6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlconfiguracoes7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        lnlConfiguracoes8.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        txtNome = view.findViewById(R.id.txtNome);
        txtNome.setText(sessao.getUsuario());

        tela = 0;

        PerfilFragment perfilFragment = new PerfilFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragmentConfiguracoes, perfilFragment, "fragmentPerfil");
        fragmentTransaction.commit();


        return view;
    }


    public void atualizarUsuario(final Usuario user) {
        String URL = getString(R.string.servidor) + "atualizar_Usuario";


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(String response) {

//                JSONObject objeto = null;


                try {
                    System.out.println("O RESPONSE É: " + response);
                    jsonobjeto = new JSONObject(response);

                    if(jsonobjeto.getInt("success") == 0) {
                        Toast.makeText(getActivity(), "O seu perfil não foi editado, tente novamente.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "O seu perfil foi editado com sucesso.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();

                parameters.put("nomeUsuario", GlobalConfiguracoes.nomeUsuario);
                parameters.put("nomeUsuarioVisualizacao", GlobalConfiguracoes.nomeUsuario);
                parameters.put("emailUsuario", GlobalConfiguracoes.emailUsuario);
                parameters.put("telefoneUsuario", GlobalConfiguracoes.telefoneUsuario);
                parameters.put("bancoUsuario", user.getBancoUsuario() + "");
                parameters.put("agenciaUsuario", GlobalConfiguracoes.agenciaUsuario);
                parameters.put("contaUsuario", GlobalConfiguracoes.contaUsuario);
                parameters.put("digitoUsuario", GlobalConfiguracoes.digitoUsuario);
                parameters.put("cpfUsuario", GlobalConfiguracoes.cpfUsuario);
                parameters.put("idUsuario", String.valueOf(sessao.getId()));

                if (base64Final != null) {
                    parameters.put("imagemUsuario", base64Final);

                } else {
                    parameters.put("imagemUsuario", base64Atual);
                }


                System.out.println("OS PARAMETROS SÃO: " + parameters);

                return parameters;
            }
        };

        requisicao.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view == btnFinalizar) {
            Usuario usuario = new Usuario();

            usuario.setNomeUsuario(Global.nomeUsuarioVisualizacao);
            usuario.setNomeUsuarioVisualizacao(Global.nomeUsuarioVisualizacao);
            usuario.setEmailUsuario(Global.emailUsuario);
            usuario.setSenhaUsuario(Global.senhaUsuario);
            usuario.setTelefoneUsuario(Global.telefoneUsuario);
            usuario.setBancoUsuario(Global.bancoUsuario);
            usuario.setContaUsuario(Global.contaUsuario);
            usuario.setDigitoUsuario(Global.digitoUsuario);
            usuario.setAgenciausuario(Global.agenciaUsuario);
            usuario.setCpfUsuario(Global.cpfUsuario);
            usuario.setImagemUsuario(Global.imagemUsurario);
            atualizarUsuario(usuario);
        } else if (view == imgFoto)

        {

            chamaImagem();
//            Intent intent = new Intent();
//            intent.setType("image/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            int PICK_IMAGE = 1;
//            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        }
    }


    private void chamaImagem() {

        //MODAL VIEW TERMOS DE USO
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogbox = getActivity().getLayoutInflater().inflate(R.layout.dialog_camera_sd, null);

        ImageView camera = dialogbox.findViewById(R.id.btn_camera);
        ImageView galeria = dialogbox.findViewById(R.id.btn_galeria);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
//        final TextView tv_titulo = dialogbox.findViewById(R.id.tv_titulo_historico_procedimento);

        builder.setView(dialogbox);
        alertDialog_show = builder.create();
        alertDialog_show.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog_show.show();


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getBaseContext().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    alertDialog_show.dismiss();
                }

            }
        });

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, RESULT_LOAD_IMG);
                alertDialog_show.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_show.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        Log.w("Entrei", "OnResult");

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            Log.w("Entrei", "Galeria");
            try {

                final Uri imageUri = data.getData();
                final Bitmap selectedImage = decodeUri(imageUri);

                InputStream inputStream = getActivity().getContentResolver().openInputStream(imageUri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                Log.w("Tamanhos da imagem", "Heigth = " + imageHeight + " : Width = " + imageWidth);
                if (imageHeight <= 150 || imageWidth <= 150) {
//                    ToolBox.ExibeMSG("Imagem muito pequena!", this);
                    Toast.makeText(getActivity(), "Imagem muito pequena, seleione outra.", Toast.LENGTH_SHORT).show();
                } else {
                    String base64 = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG);

                    System.gc();

                    GuardaImagem(selectedImage);
                    System.out.println("O BASE 64 DA GALERIA É " + base64);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Você não selecionou uma foto.", Toast.LENGTH_LONG).show();

            }


        } else if (resultCode == RESULT_OK && reqCode == REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String base64 = encodeToBase64(imageBitmap, Bitmap.CompressFormat.JPEG);

            System.gc();
            GuardaImagem(imageBitmap);

            System.out.println("O BASE 64 DA CAMERA É " + base64);
        } else {
            Toast.makeText(getActivity(), "Foto não Capturada.", Toast.LENGTH_LONG).show();
        }
        System.gc();


    }


    public Bitmap decodeUri(Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri), null, o2);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }


    private void GuardaImagem(Bitmap bitmap) {

        String nova_imagem64 = Imagem_encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG);
        System.out.println("A STRING NOVA IMAGEM É " + nova_imagem64);
        base64Final = nova_imagem64;


        if (nova_imagem64.length() > 0) {
            Bitmap bitmap2 = Imagem_decodeBase64(nova_imagem64);
            imgFoto.setImageBitmap(bitmap2);
        }

    }

    public static String Imagem_encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Função para Decodificar a imagem de String para Imagem Novamente
     */
    public static Bitmap Imagem_decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    @Override
    public void onBackPressed() {
        @SuppressLint("RestrictedApi") List<Fragment> fragmentList = getFragmentManager().getFragments();
        if (fragmentList != null) {
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof OnBackPressedListener) {
                    ((OnBackPressedListener) fragment).onBackPressed();
                }
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);

    }


    public Bitmap loadBitmap(String url) {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }


}


