package marketing.naville.chancce.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.adapter.CustomAdapterGanhos;
import marketing.naville.chancce.R;
import marketing.naville.chancce.adapter.CustomAdapterNovasOportunidades;


public class GanhosFragment extends Fragment {
    private ListView lstMeusGanhos;
    private TextView txtDesconto;
    private TextView txtTotal;

    double totalTotal = 0;
    private TextView txtAuditoriaTotal;
    String totalTotalString;

    ProgressDialog dialog;

    private void comecarLoad() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Carregando...");
        dialog.setIndeterminate(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        System.out.println("CHAMANDO GANHOS");


    }


    public String[] oportunidades;
    public String[] datas;
    public double[] valores;
    JSONArray jsonarray;
    AppShared sessao;
    RequestQueue requisicao;

    public GanhosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ganhos, container, false);

        lstMeusGanhos = (ListView) view.findViewById(R.id.lstMeusGanhos);

        txtAuditoriaTotal = (TextView) view.findViewById(R.id.txtAuditoriaTotal);

        requisicao = Volley.newRequestQueue(view.getContext());

        carregarTabela();

        sessao = new AppShared(view.getContext());


        return view;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void carregarTabela() {
        String URL = getString(R.string.servidor) + "bloco_auditado";

        comecarLoad();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                JSONObject objeto = null;

                try {

                    jsonarray = new JSONArray(response);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        int id_bloco = jsonobject.getInt("id_bloco");
                        String data_bloco = jsonobject.getString("data_bloco");
                        int qtd_canhotos = jsonobject.getInt("qtd_canhotos");
                        double valor = jsonobject.getDouble("valor");
                        int auditados = jsonobject.getInt("auditados");

                        totalTotal += valor;
                        totalTotalString = String.valueOf(totalTotal).replace(".", ",");


                        System.out.println("O ID DO BLOCO É: " + id_bloco);
                        System.out.println("A DATA DO BLOCO É: " + data_bloco);
                        System.out.println("A QUANTIDADE DE CANHOTOS É: " + qtd_canhotos);
                        System.out.println("O VALOR DO BLOCO É: " + valor);
                        System.out.println("O QUANTIDADE DE AUDITADOS É: " + auditados);


                    }

                    lstMeusGanhos.setAdapter(new CustomAdapterGanhos(getActivity(), jsonarray));
                    txtAuditoriaTotal.setText(totalTotalString);

                    if (jsonarray.length() == 0) {
                        carregarAlert();
                    }


                    dialog.dismiss();

                } catch (JSONException e) {
                    dialog.dismiss();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                System.out.println(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("id_usuario", sessao.getId() + "");


                return parameters;
            }

        };

        requisicao.add(stringRequest);
    }

    @SuppressLint("SetTextI18n")
    private void carregarAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Você não tem ganhos no momento.")
                .setCancelable(false)

                .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        txtAuditoriaTotal.setText("0,00");
    }
}
