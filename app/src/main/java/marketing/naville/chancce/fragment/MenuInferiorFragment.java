package marketing.naville.chancce.fragment;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nex3z.notificationbadge.NotificationBadge;

import marketing.naville.chancce.AppShared;
import marketing.naville.chancce.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class MenuInferiorFragment extends Fragment implements View.OnClickListener {
    private ImageView imgHome;
    private TextView txtHome;
    private ImageView imgNovos;
    private TextView txtNovos;
    private ImageView imgAbertos;
    private TextView txtAbertos;
    private ImageView imgGanhos;
    private TextView txtGanhos;
    private ImageView imgConfiguracoes;
    private TextView txtConfiguracoes;
    public int atualLigado = 0;
    public int anterior = -1;
    private ImageView[] arrayTabBarIV;
    private TextView[] arrayTabBarTxt;
    private NotificationBadge notNotifications;
    AppShared sessao;
    int sessao2;


    public MenuInferiorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_inferior, container, false);


        imgHome = (ImageView) view.findViewById(R.id.imgHome);
        txtHome = (TextView) view.findViewById(R.id.txtHome);
        imgNovos = (ImageView) view.findViewById(R.id.imgNovos);
        txtNovos = (TextView) view.findViewById(R.id.txtNovos);
        imgAbertos = (ImageView) view.findViewById(R.id.imgAbertos);
        txtAbertos = (TextView) view.findViewById(R.id.txtAbertos);
        imgGanhos = (ImageView) view.findViewById(R.id.imgGanhos);
        txtGanhos = (TextView) view.findViewById(R.id.txtGanhos);
        imgConfiguracoes = (ImageView) view.findViewById(R.id.imgConfiguracoes);
        txtConfiguracoes = (TextView) view.findViewById(R.id.txtConfiguracoes);
        notNotifications = (NotificationBadge) view.findViewById(R.id.notNotifications);

        sessao = new AppShared(getContext());

        sessao2 = sessao.getBadge();

        notNotifications.setVisibility(View.GONE);

        if (sessao2 == 1) {
            notNotifications.setVisibility(View.VISIBLE);

        }else{
            notNotifications.setVisibility(View.GONE);
        }


        imgHome.setOnClickListener(this);
        txtHome.setOnClickListener(this);
        imgNovos.setOnClickListener(this);
        txtNovos.setOnClickListener(this);
        imgAbertos.setOnClickListener(this);
        txtAbertos.setOnClickListener(this);
        imgGanhos.setOnClickListener(this);
        txtGanhos.setOnClickListener(this);
        imgConfiguracoes.setOnClickListener(this);
        txtConfiguracoes.setOnClickListener(this);


        arrayTabBarIV = new ImageView[5];
        arrayTabBarTxt = new TextView[5];


        arrayTabBarIV[0] = imgHome;
        arrayTabBarIV[1] = imgNovos;
        arrayTabBarIV[2] = imgAbertos;
        arrayTabBarIV[3] = imgGanhos;
        arrayTabBarIV[4] = imgConfiguracoes;

        arrayTabBarTxt[0] = txtHome;
        arrayTabBarTxt[1] = txtNovos;
        arrayTabBarTxt[2] = txtAbertos;
        arrayTabBarTxt[3] = txtGanhos;
        arrayTabBarTxt[4] = txtConfiguracoes;

        arrayTabBarIV[atualLigado].setColorFilter(Color.BLACK);


        //Tirar o teclado quando clicar fora
        imgHome.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        txtHome.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        imgNovos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        txtNovos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        imgAbertos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        txtAbertos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        imgGanhos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        txtGanhos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        imgConfiguracoes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        txtConfiguracoes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });


        return view;
    }

    @Override
    public void onResume() {

        sessao.getBadge();

        if (sessao2 == 1) {
            notNotifications.setVisibility(View.VISIBLE);

        }else{
            notNotifications.setVisibility(View.GONE);
        }


        super.onResume();
    }

    public void highlight(int id) {
        anterior = atualLigado;
        atualLigado = id;

        arrayTabBarIV[anterior].setColorFilter(Color.rgb(115, 115, 115));
        arrayTabBarTxt[anterior].setTextColor(Color.rgb(115, 115, 115));

        arrayTabBarIV[atualLigado].setColorFilter(Color.BLACK);
        arrayTabBarTxt[atualLigado].setTextColor(Color.BLACK);
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = null;
        if (view == imgHome) {
            fragment = new HomeFragment();
            highlight(0);

        } else if (view == imgNovos) {
            fragment = new NovasOportunidadesFragment();
            highlight(1);

        } else if (view == imgAbertos) {
            fragment = new OportunidadesAbertoFragment();
            highlight(2);


        } else if (view == imgGanhos) {
            fragment = new GanhosFragment();
            highlight(3);

        } else if (view == imgConfiguracoes) {
            fragment = new ConfiguracoesFragment();
            highlight(4);
        }
        if (view == txtHome) {
            fragment = new HomeFragment();
        } else if (view == txtNovos) {
            fragment = new NovasOportunidadesFragment();
        } else if (view == txtAbertos) {
            fragment = new OportunidadesAbertoFragment();
        } else if (view == txtGanhos) {
            fragment = new GanhosFragment();
        } else if (view == txtConfiguracoes) {
            fragment = new ConfiguracoesFragment();
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment);
        fragmentTransaction.commit();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
