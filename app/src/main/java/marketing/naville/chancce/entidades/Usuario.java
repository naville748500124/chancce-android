package marketing.naville.chancce.entidades;

import java.io.Serializable;

public class Usuario implements Serializable {
    private int IdUsuario;
    private String NomeUsuario;
    private String NomeUsuarioVisualizacao;
    private String SenhaUsuario;
    private String EmailUsuario;
    private String TelefoneUsuario;
    private int BancoUsuario;
    private String Agenciausuario;
    private String ContaUsuario;
    private String CpfUsuario;
    private String ImagemUsuario;
    private String DigitoUsuario;


    public Usuario() {
        this.IdUsuario = 0;
        this.NomeUsuario = "";
        this.SenhaUsuario = "";
        this.EmailUsuario = "";
        this.TelefoneUsuario = "";
        this.BancoUsuario = 0;
        this.Agenciausuario = "";
        this.ContaUsuario = "";
        this.CpfUsuario = "";
        this.ImagemUsuario = "";
        this.DigitoUsuario = "";

    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return NomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        NomeUsuario = nomeUsuario;
    }

    public String getNomeUsuarioVisualizacaoo() {
        return NomeUsuarioVisualizacao;
    }

    public void setNomeUsuarioVisualizacao(String nomeUsuarioVisualizacao) {
        NomeUsuarioVisualizacao = nomeUsuarioVisualizacao;
    }

    public String getSenhaUsuario() {
        return SenhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        SenhaUsuario = senhaUsuario;
    }

    public String getEmailUsuario() {
        return EmailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        EmailUsuario = emailUsuario;
    }

    public String getTelefoneUsuario() {
        return TelefoneUsuario;
    }

    public void setTelefoneUsuario(String telefoneUsuario) {
        TelefoneUsuario = telefoneUsuario;
    }

    public int getBancoUsuario() {
        return BancoUsuario;
    }

    public void setBancoUsuario(int bancoUsuario) {
        BancoUsuario = bancoUsuario;
    }

    public String getAgenciausuario() {
        return Agenciausuario;
    }

    public void setAgenciausuario(String agenciausuario) {
        Agenciausuario = agenciausuario;
    }

    public String getContaUsuario() {
        return ContaUsuario;
    }

    public void setContaUsuario(String contaUsuario) {
        ContaUsuario = contaUsuario;
    }

    public String getCpfUsuario() {
        return CpfUsuario;
    }

    public void setCpfUsuario(String cpfUsuario) {
        CpfUsuario = cpfUsuario;
    }

    public String getImagemUsuario() {
        return ImagemUsuario;
    }

    public void setImagemUsuario(String imagemUsuario) {
        ImagemUsuario = imagemUsuario;
    }

    public boolean checarEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public String getDigitoUsuario() {
        return DigitoUsuario;
    }

    public void setDigitoUsuario(String digitoUsuario) {
        DigitoUsuario = digitoUsuario;
    }
}
