package marketing.naville.chancce.entidades;

/**
 * Created by Xx on 16/06/2017.
 */

public class Ganho {
    private int IdGanho;
    private int IdUsuario;
    private int IdOportunidade;
    private String DataOportunidade;
    private float ValorGanho;
    private float DescontoGanho;

    public Ganho() {
        this.IdGanho = 0;
        this.IdUsuario = 0;
        this.IdOportunidade = 0;
        this.DataOportunidade = "";
        this.ValorGanho = 0;
        this.DescontoGanho = 0;
    }


    public int getIdGanho() {
        return IdGanho;
    }

    public void setIdGanho(int idGanho) {
        IdGanho = idGanho;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public int getIdOportunidade() {
        return IdOportunidade;
    }

    public void setIdOportunidade(int idOportunidade) {
        IdOportunidade = idOportunidade;
    }

    public String getDataOportunidade() {
        return DataOportunidade;
    }

    public void setDataOportunidade(String dataOportunidade) {
        DataOportunidade = dataOportunidade;
    }

    public float getValorGanho() {
        return ValorGanho;
    }

    public void setValorGanho(float valorGanho) {
        ValorGanho = valorGanho;
    }

    public float getDescontoGanho() {
        return DescontoGanho;
    }

    public void setDescontoGanho(float descontoGanho) {
        DescontoGanho = descontoGanho;
    }
}
