package marketing.naville.chancce.entidades;

/**
 * Created by Xx on 16/06/2017.
 */

public class Oportunidade {
    private int IdOportunidade;
    private int IdUsuario;
    private String NomeOportunidade;
    private String DataOportunidade;
    private double ValorOportunidade;
    private int[] StatusOportunidade;
    private String UrlOportunidade;

    public Oportunidade() {
        this.IdOportunidade = 0;
        this.NomeOportunidade = "";
        this.DataOportunidade = "";
        this.ValorOportunidade = 0;
        this.StatusOportunidade = null;
        this.UrlOportunidade = "";
    }

    public int getIdOportunidade() {
        return IdOportunidade;
    }

    public void setIdOportunidade(int idOportunidade) {
        IdOportunidade = idOportunidade;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getNomeOportunidade() {
        return NomeOportunidade;
    }

    public void setNomeOportunidade(String nomeOportunidade) {
        NomeOportunidade = nomeOportunidade;
    }

    public String getDataOportunidade() {
        return DataOportunidade;
    }

    public void setDataOportunidade(String dataOportunidade) {
        DataOportunidade = dataOportunidade;
    }

    public double getValorOportunidade() {
        return ValorOportunidade;
    }

    public void setValorOportunidade(double valorOportunidade) {
        ValorOportunidade = valorOportunidade;
    }

    public int[] isStatusOportunidade() {
        return StatusOportunidade;
    }

    public void setStatusOportunidade(int[] statusOportunidade) {
        StatusOportunidade = statusOportunidade;
    }

    public String getUrlOportunidade() {
        return UrlOportunidade;
    }

    public void setUrlOportunidade(String urlOportunidade) {
        UrlOportunidade = urlOportunidade;
    }
}
